package uk.co.connieprice.javasoftwarerenderer.shaders;

import uk.co.connieprice.javasoftwarerenderer.math.ColorVector;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Material;

public class NormalShader extends Shader {
	@Override
	public void RenderPixel(ColorVector PIXEL, ColorVector LIGHT, Vector3 LIGHT_DIRECTION, Vector3 CAMERA_DIRECTION, Vector3 POSITION, Vector3 NORMAL, double U, double V, Material material) {		
		PIXEL.r = (NORMAL.x / 2) + 0.5;
		PIXEL.g = (NORMAL.y / 2) + 0.5;
		PIXEL.b = (NORMAL.z / 2) + 0.5;

		PIXEL.r = U;
		PIXEL.g = V;
	}
}
