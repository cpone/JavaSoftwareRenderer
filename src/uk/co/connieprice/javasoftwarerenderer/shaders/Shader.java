package uk.co.connieprice.javasoftwarerenderer.shaders;

import java.awt.image.BufferedImage;

import uk.co.connieprice.javasoftwarerenderer.math.ColorVector;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Material;

public class Shader {
	public static ColorVector ExtractPixel(double U, double V, BufferedImage texture) {
		int textureX = (int) (texture.getWidth() * U);
		int textureY = (int) (texture.getHeight() * V);

		int argb = texture.getRGB(textureX, textureY);

		int alpha = (argb >> 24) & 0xFF;
		int red = (argb >> 16) & 0xFF;
		int green = (argb >> 8) & 0xFF;
		int blue = argb & 0xFF;

		ColorVector out = new ColorVector();
		
		out.a = alpha/255d;
		out.r = red/255d;
		out.g = green/255d;
		out.b = blue/255d;
		
		return out;
	}
	
	public void RenderPixel(ColorVector PIXEL, ColorVector LIGHT, Vector3 LIGHT_DIRECTION, Vector3 CAMERA_DIRECTION, Vector3 POSITION, Vector3 NORMAL, double U, double V, Material material) {
		PIXEL.r = U;
		PIXEL.g = V;
	}
}
