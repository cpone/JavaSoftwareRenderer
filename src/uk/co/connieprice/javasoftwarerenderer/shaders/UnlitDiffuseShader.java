package uk.co.connieprice.javasoftwarerenderer.shaders;

import uk.co.connieprice.javasoftwarerenderer.math.ColorVector;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Material;

public class UnlitDiffuseShader extends Shader {
	@Override
	public void RenderPixel(ColorVector PIXEL, ColorVector LIGHT, Vector3 LIGHT_DIRECTION, Vector3 CAMERA_DIRECTION, Vector3 POSITION, Vector3 NORMAL, double U, double V, Material material) {		
		ColorVector diffusePixel = Shader.ExtractPixel(U, V, material.getTexture("diffuse"));
		
		PIXEL.a = diffusePixel.a;
		PIXEL.r = diffusePixel.r;
		PIXEL.g = diffusePixel.g;
		PIXEL.b = diffusePixel.b;
	}
}
