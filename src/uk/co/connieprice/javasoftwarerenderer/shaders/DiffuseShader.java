package uk.co.connieprice.javasoftwarerenderer.shaders;

import uk.co.connieprice.javasoftwarerenderer.math.ColorVector;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Material;

public class DiffuseShader extends Shader {
	@Override
	public void RenderPixel(ColorVector PIXEL, ColorVector LIGHT, Vector3 LIGHT_DIRECTION, Vector3 CAMERA_DIRECTION, Vector3 POSITION, Vector3 NORMAL, double U, double V, Material material) {		
//		Vector3 reverseLightDirection = LIGHT_DIRECTION.copy().invert();
//		double cosTheta = NORMAL.dotProduct(reverseLightDirection);
//		cosTheta = Math.max(0, Math.min(1, cosTheta));
//
//		Vector3 reflection = LIGHT_DIRECTION.copy().subtract(NORMAL.copy().multiply(2 * LIGHT_DIRECTION.dotProduct(NORMAL)));
//		
//		double cosAlpha = 1 - CAMERA_DIRECTION.dotProduct(reflection);
//		cosAlpha = Math.max(0, Math.min(1, cosAlpha));
//		
//		cosAlpha = 0.2 + (cosAlpha * 0.8);
		
//		ColorVector diffusePixel = Shader.ExtractPixel(U, V, material.getTexture("diffuse"));
		
//		PIXEL.multiply(diffusePixel).multiply(LIGHT);
//		PIXEL.multiply(ColorVector.WHITE).multiply(LIGHT).multiply(Math.pow(cosAlpha, 1));
		
		ColorVector diffusePixel = Shader.ExtractPixel(U, V, material.getTexture("diffuse"));
		
		PIXEL.a = diffusePixel.a;
		PIXEL.r = diffusePixel.r * LIGHT.r;
		PIXEL.g = diffusePixel.g * LIGHT.g;
		PIXEL.b = diffusePixel.b * LIGHT.b;
//		
//		PIXEL.r = (NORMAL.x / 2) + 0.5;
//		PIXEL.g = (NORMAL.y / 2) + 0.5;
//		PIXEL.b = (NORMAL.z / 2) + 0.5;
		
//		PIXEL.r = U;
//		PIXEL.g = V;
	}
}
