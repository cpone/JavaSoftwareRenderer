package uk.co.connieprice.javasoftwarerenderer.tests.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import uk.co.connieprice.javasoftwarerenderer.math.Matrix;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;

class MatrixTest {
	@Test
	void testMultiply() {
		Matrix matrix1 = new Matrix(2, 2);
		matrix1.setElement(0, 0, 0); matrix1.setElement(0, 1, 3);
		matrix1.setElement(1, 0, 4); matrix1.setElement(1, 1, 2);
		
		Matrix matrix2 = new Matrix(2, 2);
		matrix2.setElement(0, 0, 2); matrix2.setElement(0, 1, 4);
		matrix2.setElement(1, 0, 2); matrix2.setElement(1, 1, 8);
		
		Matrix output = matrix1.multiply(matrix2);
		
		assertEquals(output.getElement(0, 0), 6); assertEquals(output.getElement(0, 1), 24);
		assertEquals(output.getElement(1, 0), 12); assertEquals(output.getElement(1, 1), 32);
	}
	
	@Test
	void testVectorMultiply() {
		Matrix scale = new Matrix(4, 4);
		scale.setElement(0, 0, 2); scale.setElement(0, 1, 0); scale.setElement(0, 2, 0); scale.setElement(0, 3, 0);
		scale.setElement(1, 0, 0); scale.setElement(1, 1, 2); scale.setElement(1, 2, 0); scale.setElement(1, 3, 0);
		scale.setElement(2, 0, 0); scale.setElement(2, 1, 0); scale.setElement(2, 2, 2); scale.setElement(2, 3, 0);
		scale.setElement(3, 0, 0); scale.setElement(3, 1, 0); scale.setElement(3, 2, 0); scale.setElement(3, 3, 1);
		
		Vector3 vector = new Vector3(2, 4, 5);
		vector.multiply(scale);
		
		assertEquals(vector.x, 4);
		assertEquals(vector.y, 8);
		assertEquals(vector.z, 10);
	}
}