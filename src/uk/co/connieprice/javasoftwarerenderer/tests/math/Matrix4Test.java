package uk.co.connieprice.javasoftwarerenderer.tests.math;

import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import uk.co.connieprice.javasoftwarerenderer.math.Euler;
import uk.co.connieprice.javasoftwarerenderer.math.Matrix4;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;

class Matrix4Test {
	@Test
	void identity() {
		Matrix4 identityMatrix = new Matrix4(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		);
		Matrix4 result = Matrix4.TRANSFORMLESS;
		assertEquals(identityMatrix, result);
	}
	
	@Test
	void translation() {
		Vector3 translation = new Vector3(2, 3, 4);
		Matrix4 translationMatrix = Matrix4.FromTranslation(translation);
		Matrix4 expectedMatrix = new Matrix4(
			1, 0, 0, 2,
			0, 1, 0, 3,
			0, 0, 1, 4,
			0, 0, 0, 1
		);
		Matrix4 result = translationMatrix;
		assertEquals(expectedMatrix, result);
	}
	
	@Test
	void eulerRotation() {
		Euler rotation = new Euler(Math.PI/2, Math.PI, Math.PI/4);
		Matrix4 rotationMatrix = Matrix4.FromEuler(rotation);
		// Define the expected matrix values based on the given Euler rotation angles
		Matrix4 expectedMatrix = new Matrix4(
			-0.707107, 0.707107,  0.000000, 0.000000,
			 0.000000, 0.000000,  1.000000, 0.000000,
			 0.707107, 0.707107, -0.000000, 0.000000,
			 0.000000, 0.000000,  0.000000, 1.000000
		);
		Matrix4 result = rotationMatrix;
		assertEquals(expectedMatrix, result);
	}
	
	@Test
	void scale() {
		Vector3 scale = new Vector3(2, 3, 4);
		Matrix4 scaleMatrix = Matrix4.FromScale(scale);
		Matrix4 expectedMatrix = new Matrix4(
			2, 0, 0, 0,
			0, 3, 0, 0,
			0, 0, 4, 0,
			0, 0, 0, 1
		);
		Matrix4 result = scaleMatrix;
		assertEquals(expectedMatrix, result);
	}
	
	private Matrix4 matrix1;
	private Matrix4 matrix2;

	@BeforeEach
	void setup() {
		matrix1 = new Matrix4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
		matrix2 = new Matrix4(17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32);
	}

	@Test
	void testCopy() {
		Matrix4 copy = matrix1.copy();
		assertNotSame(matrix1, copy);
		assertEquals(matrix1.m11, copy.m11);
		assertEquals(matrix1.m12, copy.m12);
		assertEquals(matrix1.m13, copy.m13);
		assertEquals(matrix1.m14, copy.m14);
		assertEquals(matrix1.m21, copy.m21);
		assertEquals(matrix1.m22, copy.m22);
		assertEquals(matrix1.m23, copy.m23);
		assertEquals(matrix1.m24, copy.m24);
		assertEquals(matrix1.m31, copy.m31);
		assertEquals(matrix1.m32, copy.m32);
		assertEquals(matrix1.m33, copy.m33);
		assertEquals(matrix1.m34, copy.m34);
		assertEquals(matrix1.m41, copy.m41);
		assertEquals(matrix1.m42, copy.m42);
		assertEquals(matrix1.m43, copy.m43);
		assertEquals(matrix1.m44, copy.m44);
	}
	
	@Test
	void testSet() {
		Matrix4 copy = new Matrix4();
		copy.set(matrix1);
		assertEquals(matrix1.m11, copy.m11);
		assertEquals(matrix1.m12, copy.m12);
		assertEquals(matrix1.m13, copy.m13);
		assertEquals(matrix1.m14, copy.m14);
		assertEquals(matrix1.m21, copy.m21);
		assertEquals(matrix1.m22, copy.m22);
		assertEquals(matrix1.m23, copy.m23);
		assertEquals(matrix1.m24, copy.m24);
		assertEquals(matrix1.m31, copy.m31);
		assertEquals(matrix1.m32, copy.m32);
		assertEquals(matrix1.m33, copy.m33);
		assertEquals(matrix1.m34, copy.m34);
		assertEquals(matrix1.m41, copy.m41);
		assertEquals(matrix1.m42, copy.m42);
		assertEquals(matrix1.m43, copy.m43);
		assertEquals(matrix1.m44, copy.m44);
	}
	
	@Test
	void testAdd() {
		Matrix4 matrixResult = matrix1.add(matrix2);
		
		assertEquals(matrixResult.m11, matrix1.m11 + matrix2.m11);
		assertEquals(matrixResult.m12, matrix1.m12 + matrix2.m12);
		assertEquals(matrixResult.m13, matrix1.m13 + matrix2.m13);
		assertEquals(matrixResult.m14, matrix1.m14 + matrix2.m14);

		assertEquals(matrixResult.m21, matrix1.m21 + matrix2.m21);
		assertEquals(matrixResult.m22, matrix1.m22 + matrix2.m22);
		assertEquals(matrixResult.m23, matrix1.m23 + matrix2.m23);
		assertEquals(matrixResult.m24, matrix1.m24 + matrix2.m24);
		
		assertEquals(matrixResult.m31, matrix1.m31 + matrix2.m31);
		assertEquals(matrixResult.m32, matrix1.m32 + matrix2.m32);
		assertEquals(matrixResult.m33, matrix1.m33 + matrix2.m33);
		assertEquals(matrixResult.m34, matrix1.m34 + matrix2.m34);
		
		assertEquals(matrixResult.m41, matrix1.m41 + matrix2.m41);
		assertEquals(matrixResult.m42, matrix1.m42 + matrix2.m42);
		assertEquals(matrixResult.m43, matrix1.m43 + matrix2.m43);
		assertEquals(matrixResult.m44, matrix1.m44 + matrix2.m44);
	}
	
	@Test
	void testSubtract() {
		Matrix4 matrixResult = matrix1.subtract(matrix2);
		
		assertEquals(matrixResult.m11, matrix1.m11 - matrix2.m11);
		assertEquals(matrixResult.m12, matrix1.m12 - matrix2.m12);
		assertEquals(matrixResult.m13, matrix1.m13 - matrix2.m13);
		assertEquals(matrixResult.m14, matrix1.m14 - matrix2.m14);

		assertEquals(matrixResult.m21, matrix1.m21 - matrix2.m21);
		assertEquals(matrixResult.m22, matrix1.m22 - matrix2.m22);
		assertEquals(matrixResult.m23, matrix1.m23 - matrix2.m23);
		assertEquals(matrixResult.m24, matrix1.m24 - matrix2.m24);
		
		assertEquals(matrixResult.m31, matrix1.m31 - matrix2.m31);
		assertEquals(matrixResult.m32, matrix1.m32 - matrix2.m32);
		assertEquals(matrixResult.m33, matrix1.m33 - matrix2.m33);
		assertEquals(matrixResult.m34, matrix1.m34 - matrix2.m34);
		
		assertEquals(matrixResult.m41, matrix1.m41 - matrix2.m41);
		assertEquals(matrixResult.m42, matrix1.m42 - matrix2.m42);
		assertEquals(matrixResult.m43, matrix1.m43 - matrix2.m43);
		assertEquals(matrixResult.m44, matrix1.m44 - matrix2.m44);
	}
	
	@Test
	void testMultiply() {
		Matrix4 matrixResult = matrix1.multiply(matrix2);

		assertEquals(matrixResult.m11, matrix1.m11 * matrix2.m11 + matrix1.m12 * matrix2.m21 + matrix1.m13 * matrix2.m31 + matrix1.m14 * matrix2.m41);
		assertEquals(matrixResult.m12, matrix1.m11 * matrix2.m12 + matrix1.m12 * matrix2.m22 + matrix1.m13 * matrix2.m32 + matrix1.m14 * matrix2.m42);
		assertEquals(matrixResult.m13, matrix1.m11 * matrix2.m13 + matrix1.m12 * matrix2.m23 + matrix1.m13 * matrix2.m33 + matrix1.m14 * matrix2.m43);
		assertEquals(matrixResult.m14, matrix1.m11 * matrix2.m14 + matrix1.m12 * matrix2.m24 + matrix1.m13 * matrix2.m34 + matrix1.m14 * matrix2.m44);
		assertEquals(matrixResult.m21, matrix1.m21 * matrix2.m11 + matrix1.m22 * matrix2.m21 + matrix1.m23 * matrix2.m31 + matrix1.m24 * matrix2.m41);
		assertEquals(matrixResult.m22, matrix1.m21 * matrix2.m12 + matrix1.m22 * matrix2.m22 + matrix1.m23 * matrix2.m32 + matrix1.m24 * matrix2.m42);
		assertEquals(matrixResult.m23, matrix1.m21 * matrix2.m13 + matrix1.m22 * matrix2.m23 + matrix1.m23 * matrix2.m33 + matrix1.m24 * matrix2.m43);
		assertEquals(matrixResult.m24, matrix1.m21 * matrix2.m14 + matrix1.m22 * matrix2.m24 + matrix1.m23 * matrix2.m34 + matrix1.m24 * matrix2.m44);
		assertEquals(matrixResult.m31, matrix1.m31 * matrix2.m11 + matrix1.m32 * matrix2.m21 + matrix1.m33 * matrix2.m31 + matrix1.m34 * matrix2.m41);
		assertEquals(matrixResult.m32, matrix1.m31 * matrix2.m12 + matrix1.m32 * matrix2.m22 + matrix1.m33 * matrix2.m32 + matrix1.m34 * matrix2.m42);
		assertEquals(matrixResult.m33, matrix1.m31 * matrix2.m13 + matrix1.m32 * matrix2.m23 + matrix1.m33 * matrix2.m33 + matrix1.m34 * matrix2.m43);
		assertEquals(matrixResult.m34, matrix1.m31 * matrix2.m14 + matrix1.m32 * matrix2.m24 + matrix1.m33 * matrix2.m34 + matrix1.m34 * matrix2.m44);
		assertEquals(matrixResult.m41, matrix1.m41 * matrix2.m11 + matrix1.m42 * matrix2.m21 + matrix1.m43 * matrix2.m31 + matrix1.m44 * matrix2.m41);
		assertEquals(matrixResult.m42, matrix1.m41 * matrix2.m12 + matrix1.m42 * matrix2.m22 + matrix1.m43 * matrix2.m32 + matrix1.m44 * matrix2.m42);
		assertEquals(matrixResult.m43, matrix1.m41 * matrix2.m13 + matrix1.m42 * matrix2.m23 + matrix1.m43 * matrix2.m33 + matrix1.m44 * matrix2.m43);
		assertEquals(matrixResult.m44, matrix1.m41 * matrix2.m14 + matrix1.m42 * matrix2.m24 + matrix1.m43 * matrix2.m34 + matrix1.m44 * matrix2.m44);
	}
}