package uk.co.connieprice.javasoftwarerenderer.tests.objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import uk.co.connieprice.javasoftwarerenderer.math.Euler;
import uk.co.connieprice.javasoftwarerenderer.math.Vector2;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;
import uk.co.connieprice.javasoftwarerenderer.objects.Camera;

class CameraTest {
	private Camera camera;
	private Camera rotatedCamera;

	@BeforeEach
	void setup() {
		camera = new Camera().SetOrthographic(6.4, 4.8, 0, 200);
		
		rotatedCamera = new Camera().SetOrthographic(6.4, 4.8, 0, 200);
		rotatedCamera.setRotation(0, Math.PI/2, 0);
		rotatedCamera.updateTransformationMatrix();
	}

	@Test
	void testWorldToScreen() {
		assertEquals(new Vector3( 0.0,  0.0, 1.0), camera.worldToScreen(new Vector3( 0.0,  0.0, 0.0)));
		assertEquals(new Vector3(-1.0,  0.0, 1.0), camera.worldToScreen(new Vector3(-3.2,  0.0, 0.0)));
		assertEquals(new Vector3(-1.0, -1.0, 1.0), camera.worldToScreen(new Vector3(-3.2, -2.4, 0.0)));
		assertEquals(new Vector3( 0.0, -1.0, 1.0), camera.worldToScreen(new Vector3( 0.0, -2.4, 0.0)));

		assertEquals(new Vector3( 0.0,  1.0, 1.0), camera.worldToScreen(new Vector3( 0.0,  2.4, 0.0)));
		assertEquals(new Vector3( 1.0,  1.0, 1.0), camera.worldToScreen(new Vector3( 3.2,  2.4, 0.0)));
		assertEquals(new Vector3( 1.0,  0.0, 1.0), camera.worldToScreen(new Vector3( 3.2,  0.0, 0.0)));
	}

//	@Test
//	void testRotatedWorldToScreen() {
//		assertEquals(new Vector3( 0.0,  0.0, -1.0), rotatedCamera.worldToScreen(new Vector3( 0.0,  0.0,  0.0)));
//		assertEquals(new Vector3( 1.0,  0.0, -1.0), rotatedCamera.worldToScreen(new Vector3( 0.0,  0.0, -3.2)));
//		assertEquals(new Vector3( 1.0, -1.0, -1.0), rotatedCamera.worldToScreen(new Vector3( 0.0, -2.4, -3.2)));
//		assertEquals(new Vector3( 0.0, -1.0, -1.0), rotatedCamera.worldToScreen(new Vector3( 0.0, -2.4,  0.0)));
//
//		assertEquals(new Vector3( 0.0,  1.0, -1.0), rotatedCamera.worldToScreen(new Vector3( 0.0,  2.4,  0.0)));
//		assertEquals(new Vector3(-1.0,  1.0, -1.0), rotatedCamera.worldToScreen(new Vector3( 0.0,  2.4,  3.2)));
//		assertEquals(new Vector3(-1.0,  0.0, -1.0), rotatedCamera.worldToScreen(new Vector3( 0.0,  0.0,  3.2)));
//	}
	
	@Test
	void testScreenToWorld() {
		assertEquals(new Vector3( 0.0,  0.0, 200.0), camera.screenToWorld(new Vector3( 0.0,  0.0, -1.0)));
		assertEquals(new Vector3(-3.2,  0.0, 200.0), camera.screenToWorld(new Vector3(-1.0,  0.0, -1.0)));
		assertEquals(new Vector3(-3.2, -2.4, 200.0), camera.screenToWorld(new Vector3(-1.0, -1.0, -1.0)));
		assertEquals(new Vector3( 0.0, -2.4, 200.0), camera.screenToWorld(new Vector3( 0.0, -1.0, -1.0)));

		assertEquals(new Vector3( 0.0,  2.4, 200.0), camera.screenToWorld(new Vector3( 0.0,  1.0, -1.0)));
		assertEquals(new Vector3( 3.2,  2.4, 200.0), camera.screenToWorld(new Vector3( 1.0,  1.0, -1.0)));
		assertEquals(new Vector3( 3.2,  0.0, 200.0), camera.screenToWorld(new Vector3( 1.0,  0.0, -1.0)));
	}
}
