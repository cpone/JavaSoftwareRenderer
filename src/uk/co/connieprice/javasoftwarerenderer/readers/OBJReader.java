package uk.co.connieprice.javasoftwarerenderer.readers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import uk.co.connieprice.javasoftwarerenderer.math.Vector2;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;
import uk.co.connieprice.javasoftwarerenderer.objects.Model;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Material;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Triangle;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Vertex;
import uk.co.connieprice.javasoftwarerenderer.shaders.DiffuseShader;

public class OBJReader {
	public static class MTLMaterial {
		String name;
		HashMap<String, String> textures = new HashMap<String, String>();
	}
	
	public static void readMTL(Model model, String filepath) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filepath));

			List<MTLMaterial> materials = new ArrayList<MTLMaterial>();
			MTLMaterial lastMaterial = new MTLMaterial();

			String line;
			while ((line = reader.readLine()) != null) {
			    String[] parts = line.split(" ");

			    if (parts.length > 0) {
			        String command = parts[0];

			        switch(command) {
			        	case "newmtl":
			        		lastMaterial = new MTLMaterial();
			        		lastMaterial.name = parts[1];
			        		materials.add(lastMaterial);
			        		break;
			        	case "map_Kd":
			        		lastMaterial.textures.put("diffuse", parts[1]);
			        		break;
			        }
			    }
			}

			for (int i = 0; i < materials.size(); i++) {
				MTLMaterial material = materials.get(i);

				Material modelMaterial = new Material(new DiffuseShader());
				for (Entry<String, String> entry : material.textures.entrySet()) {
				    String name = entry.getKey();
				    String path = entry.getValue();
				    
				    modelMaterial.addTexture(name, path);
				}
				
				model.addMaterial(material.name, modelMaterial);
				
			}
			
		    reader.close();
		} catch (FileNotFoundException e1) {

		} catch (IOException e) {

		}
	}
	
	public static void readOBJ(Model model, String filepath) {	
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filepath));

			String currentMaterial = null;
			List<Vector3> vertexes = new ArrayList<Vector3>();
			List<Vector3> textureCoordinates = new ArrayList<Vector3>();
			List<Vector3> vertexNormals = new ArrayList<Vector3>();
	
			String line;
			while ((line = reader.readLine()) != null) {
			    String[] parts = line.split(" ");

			    if (parts.length > 0) {
			        String command = parts[0];

			        switch(command) {
			        	case "mtllib":
			        		readMTL(model, parts[1]);
			        		break;
			        	case "usemtl":
			        		currentMaterial = parts[1];
			        		break;
			        	case "v":
			        		vertexes.add(new Vector3(
			        			Double.parseDouble(parts[1]),
			        			Double.parseDouble(parts[2]),
			        			Double.parseDouble(parts[3])
			        		));
			        		break;
			        	case "vt":
			        		if (parts.length <= 3) {
				        		textureCoordinates.add(new Vector3(
				        			Double.parseDouble(parts[1]),
				        			Double.parseDouble(parts[2]),
				        			0
				        		));
			        		} else {
			        			textureCoordinates.add(new Vector3(
				        			Double.parseDouble(parts[1]),
				        			Double.parseDouble(parts[2]),
				        			Double.parseDouble(parts[3])
				        		));
			        		}
			        		break;
			        	case "vn":
			        		vertexNormals.add(new Vector3(
			        			Double.parseDouble(parts[1]),
			        			Double.parseDouble(parts[2]),
			        			Double.parseDouble(parts[3])
			        		));
			        		break;
			        	case "f":
			        		int vertexCount = parts.length - 1;
			        		
			        		int[] vertexIndices = new int[vertexCount];
			        		
			        		boolean usingTextureCoordinates = false;
			        		int[] vertexTextureCoordinateIndices = new int[vertexCount];
			        		
			        		boolean usingVertexNormals = false;
			        		int[] vertexNormalIndices = new int[vertexCount];

			        		for (int i = 0; i < vertexCount; i++) {
			        			String[] vertexParts = parts[i+1].split("/");
			        			
			        			vertexIndices[i] = Integer.parseInt(vertexParts[0]) - 1;

			        			if (vertexParts.length > 1) {
			        				if (!vertexParts[1].equals("")) {
			        					vertexTextureCoordinateIndices[i] = Integer.parseInt(vertexParts[1]) - 1;
			        					usingTextureCoordinates = true;
			        				}

			        				if (vertexParts.length > 2) {
				        				if (vertexParts[2] != "") {
				        					vertexNormalIndices[i] = Integer.parseInt(vertexParts[2]) - 1;
				        					usingVertexNormals = true;
				        				}
			        				}
			        			}
			        		}

			        		Vertex[] polygonVertexes = new Vertex[vertexCount];
			        		for (int i = 0; i < vertexCount; i++) {
			        			polygonVertexes[i] = new Vertex(
			        					vertexes.get(vertexIndices[i]),
			        					usingTextureCoordinates ? textureCoordinates.get(vertexTextureCoordinateIndices[i]) : new Vector3(),
			        					usingVertexNormals ? vertexNormals.get(vertexNormalIndices[i]) : new Vector3()
			        			);
			        		}
			        		
			        		if (vertexCount == 3) {
			        			model.addTriangle(new Triangle(
		        					polygonVertexes[0],
		        					polygonVertexes[1],
		        					polygonVertexes[2],
		        					currentMaterial
			    		        ));
			        		}
			        		
			        		break;
			        }
			    }
			}

		    reader.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    
	}
}
