package uk.co.connieprice.javasoftwarerenderer.math;

public class ColorVector implements Vector<ColorVector> {
	public final static ColorVector RED = new ColorVector(1, 0, 0);
	public final static ColorVector GREEN = new ColorVector(0, 1, 0);
	public final static ColorVector BLUE = new ColorVector(0, 0, 1);

	public final static ColorVector WHITE = new ColorVector(1, 1, 1);
	public final static ColorVector BLACK = new ColorVector(0, 0, 0);

	public double r;
	public double g;
	public double b;
	public double a;

	public ColorVector() {}
	public ColorVector(double r, double g, double b) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = 1;
	}
	public ColorVector(ColorVector color) {
		this.set(color);
	}
	
	@Override
	public boolean equals(Object object) {
	    if (this == object) return true;
	    if (object == null || getClass() != object.getClass()) return false;

	    ColorVector color = (ColorVector) object;
	    return this.r == color.r && this.g == color.g && this.b == color.b && this.a == color.a;
	}
	
	@Override
	public ColorVector copy() {
		return new ColorVector(this);
	}
	
	@Override
	public double length() {
		return Math.sqrt(this.squareLength());
	}
	
	@Override
	public double squareLength() {
		return r * r + g * g + b * b + a * a;
	}
	
	@Override
	public ColorVector setLength(double length) {
		return this.setSquareLength(length * length);
	}
	
	@Override
	public ColorVector setSquareLength(double squareLength) {
		double oldSquareLength = this.squareLength();
		return (oldSquareLength == 0 || oldSquareLength == squareLength) ? this : this.multiply(Math.sqrt(squareLength / oldSquareLength));
	}
	
	@Override
	public ColorVector set(ColorVector color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = color.a;
		return this;
	}
	
	@Override
	public ColorVector setZero() {
		this.r = 0;
		this.g = 0;
		this.b = 0;
		this.a = 0;
		return this;
	}
	
	@Override
	public ColorVector add(ColorVector color) {
		this.r += color.r;
		this.g += color.g;
		this.b += color.b;
		this.a += color.a;
		return this;
	}
	
	@Override
	public ColorVector subtract(ColorVector color) {
		this.r -= color.r;
		this.g -= color.g;
		this.b -= color.b;
		this.a -= color.a;
		return this;
	}
	
	@Override
	public ColorVector normalize() {
		double length = this.length();
		if (length != 0) {
			this.r /= length;
			this.g /= length;
			this.b /= length;
			this.a /= length;
		}
		return this;
	}
	
	@Override
	public ColorVector multiply(double value) {
		this.r *= value;
		this.g *= value;
		this.b *= value;
		this.a *= value;
		return this;
	}
	
	@Override
	public ColorVector multiply(ColorVector color) {
		this.r *= color.r;
		this.g *= color.g;
		this.b *= color.b;
		this.a *= color.a;
		return this;
	}
	
	@Override
	public ColorVector multiply(Matrix matrix) {
		Matrix vector = new Matrix(4, 1);
		vector.setElement(0, 0, this.r);
		vector.setElement(1, 0, this.g);
		vector.setElement(2, 0, this.b);
		vector.setElement(3, 0, this.a);
		
		Matrix output = matrix.multiply(vector);
		this.r = output.getElement(0, 0);
		this.g = output.getElement(1, 0);
		this.b = output.getElement(2, 0);
		this.a = output.getElement(3, 0);
		
		return this;
	}
	
	@Override
	public ColorVector multiply(Matrix4 matrix) {
		this.r = r * matrix.m11 + g * matrix.m12 + b * matrix.m13 + a * matrix.m14;
		this.g = r * matrix.m21 + g * matrix.m22 + b * matrix.m23 + a * matrix.m24;
		this.b = r * matrix.m31 + g * matrix.m32 + b * matrix.m33 + a * matrix.m34;
		this.a = r * matrix.m41 + g * matrix.m42 + b * matrix.m43 + a * matrix.m44;

		return this;
	}
	
	@Override
	public ColorVector invert() {
		return this.multiply(-1);
	}
	
	@Override
	public ColorVector divide(double value) {
		this.r /= value;
		this.g /= value;
		this.b /= value;
		this.a /= value;
		return this;
	}
	
	@Override
	public ColorVector divide(ColorVector color) {
		this.r /= color.r;
		this.g /= color.g;
		this.b /= color.b;
		this.a /= color.a;
		return this;
	}
	
	@Override
	public ColorVector power(double value) {
		this.r = Math.pow(this.r, value);
		this.g = Math.pow(this.g, value);
		this.b = Math.pow(this.b, value);
		this.a = Math.pow(this.a, value);
		return this;
	}
	
	@Override
	public ColorVector lerp(ColorVector vector, double value) {
		this.r = this.r + (vector.r - this.r) * value;
		this.g = this.g + (vector.g - this.g) * value;
		this.b = this.b + (vector.b - this.b) * value;
		this.a = this.a + (vector.a - this.a) * value;
		return this;
	}
	
	@Override
	public ColorVector vectorDistance(ColorVector vector) {
		ColorVector output = new ColorVector();
		output.r = vector.r - this.r;
		output.g = vector.g - this.g;
		output.b = vector.b - this.b;
		output.a = vector.a - this.a;
		return output;
	}
	
	@Override
	public double distance(ColorVector vector) {
		return Math.sqrt(this.squareDistance(vector));
	}
	
	@Override
	public double squareDistance(ColorVector color) {
		final double deltaR = color.r - this.r;
		final double deltaG = color.g - this.g;
		final double deltaB = color.b - this.b;
		final double deltaA = color.a - this.a;
		return deltaR * deltaR + deltaG * deltaG + deltaB * deltaB + deltaA * deltaA;
	}
	
	public ColorVector crossProduct(ColorVector color) {
		return this;
	}
	
	@Override
	public double dotProduct(ColorVector color) {
		return (this.r * color.r) + (this.g * color.g) + (this.b * color.b) + (this.a * color.a);
	}

	public int getARGB() {
		int argb = (int) (a * 255);
		argb = (argb << 8) + (int) (r * 255);
		argb = (argb << 8) + (int) (g * 255);
		argb = (argb << 8) + (int) (b * 255);
		
		return argb;
	}
}
