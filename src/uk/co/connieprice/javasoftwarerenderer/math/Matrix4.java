package uk.co.connieprice.javasoftwarerenderer.math;


// TODO Make Matrix4 because hardcoding is faster sometimes.
public class Matrix4 {
	public static final Matrix4 TRANSFORMLESS = new Matrix4(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);

	public static Matrix4 FromTranslation(Vector3 translation) {
		return new Matrix4(
			1, 0, 0, translation.x,
			0, 1, 0, translation.y,
			0, 0, 1, translation.z,
			0, 0, 0,             1
		);
	}

	// Expanded Euler matrices so that no matrix multiplication is required to take place.
	public static Matrix4 FromEuler(Euler rotation) {
		double pitch = rotation.pitch;
		double yaw = rotation.yaw;
		double roll = rotation.roll;

		double cosPitch = Math.cos(pitch);
		double cosYaw = Math.cos(yaw);
		double cosRoll = Math.cos(roll);
		
		double sinPitch = Math.sin(pitch);
		double sinYaw = Math.sin(yaw);
		double sinRoll = Math.sin(roll);
		
		double m11 = cosYaw * cosRoll;
		double m12 = cosYaw * sinRoll * -1;
		double m13 = sinYaw;
		double m14 = 0;

		double m21 = (sinPitch * sinYaw * cosRoll) + (cosPitch * sinRoll);
		double m22 = (sinPitch * sinYaw * sinRoll) + (cosPitch * cosRoll);
		double m23 = sinPitch * cosYaw * -1;
		double m24 = 0;

		double m31 = (cosPitch * sinYaw * cosRoll * -1) + (sinPitch * sinRoll);
		double m32 = (cosPitch * sinYaw * sinRoll)      + (sinPitch * cosRoll);
		double m33 = cosPitch * cosYaw;
		double m34 = 0;

		double m41 = 0;
		double m42 = 0;
		double m43 = 0;
		double m44 = 1;

		Matrix4 rotationMatrix = new Matrix4(
			m11, m12, m13, m14,
			m21, m22, m23, m24,
			m31, m32, m33, m34,
			m41, m42, m43, m44
		);
		
		return rotationMatrix;
	}

	public static Matrix4 FromScale(Vector3 scale) {
		return new Matrix4(
			scale.x,       0,       0, 0,
			      0, scale.y,       0, 0,
			      0,       0, scale.z, 0,
			      0,       0,       0, 1
		);
	}

	public static Matrix4 OrthographicProjection(double top, double bottom, double left, double right, double near, double far) {
		double m11 = 2 / (right - left);
	    double m12 = 0;
	    double m13 = 0;
	    double m14 = -(right + left) / (right - left);

	    double m21 = 0;
	    double m22 = 2 / (top - bottom);
	    double m23 = 0;
	    double m24 = -(top + bottom) / (top - bottom);

	    double m31 = 0;
	    double m32 = 0;
	    double m33 = -2 / (far - near);
	    double m34 = (far + near) / (far - near);

	    double m41 = 0;
	    double m42 = 0;
	    double m43 = 0;
	    double m44 = 1;

		return new Matrix4(
			m11, m12, m13, m14,
			m21, m22, m23, m24,
			m31, m32, m33, m34,
			m41, m42, m43, m44
		);
	}

	public static Matrix4 PerspectiveProjection(double FOV, double aspectRatio, double near, double far) {
		double m11 = 1 / (aspectRatio * Math.tan(FOV/2));
		double m12 = 0;
		double m13 = 0;
		double m14 = 0;

		double m21 = 0;
		double m22 = 1 / Math.tan(FOV/2);
		double m23 = 0;
		double m24 = 0;

		double m31 = 0;
		double m32 = 0;
		double m33 = -(far + near) / (far - near);
	    double m34 = -((2 * far * near) / (far - near));

		double m41 = 0;
		double m42 = 0;
		double m43 = -1;
		double m44 = 1;

		return new Matrix4(
			m11, m12, m13, m14,
			m21, m22, m23, m24,
			m31, m32, m33, m34,
			m41, m42, m43, m44
		);
	}
	
	public double m11;
	public double m12;
	public double m13;
	public double m14;
	
	public double m21;
	public double m22;
	public double m23;
	public double m24;
	
	public double m31;
	public double m32;
	public double m33;
	public double m34;
	
	public double m41;
	public double m42;
	public double m43;
	public double m44;

	public Matrix4(
			double m11,
			double m12,
			double m13,
			double m14,
			double m21,
			double m22,
			double m23,
			double m24,
			double m31,
			double m32,
			double m33,
			double m34,
			double m41,
			double m42,
			double m43,
			double m44
	) {
		this.m11 = m11;
		this.m12 = m12;
		this.m13 = m13;
		this.m14 = m14;
		
		this.m21 = m21;
		this.m22 = m22;
		this.m23 = m23;
		this.m24 = m24;
		
		this.m31 = m31;
		this.m32 = m32;
		this.m33 = m33;
		this.m34 = m34;
		
		this.m41 = m41;
		this.m42 = m42;
		this.m43 = m43;
		this.m44 = m44;
	}
	
	public Matrix4() {}

	public Matrix4(Matrix4 matrix) {
		this.set(matrix);
	}
	
	public String toString() {
        return String.format("Matrix4( "
        		+ "%f, %f, %f, %f, "
        		+ "%f, %f, %f, %f, "
        		+ "%f, %f, %f, %f, "
        		+ "%f, %f, %f, %f "
        		+ ")",
        		this.m11, this.m12, this.m13, this.m14,
        		this.m21, this.m22, this.m23, this.m24,
        		this.m31, this.m32, this.m33, this.m34,
        		this.m41, this.m42, this.m43, this.m44
        );
    }
	
	private static double equalsEpsilon = 0.00001;
	@Override
	public boolean equals(Object object) {
	    if (this == object) return true;
	    if (object == null || getClass() != object.getClass()) return false;

	    Matrix4 matrix = (Matrix4) object;
	    
	    boolean m11Equal = Math.abs(this.m11 - matrix.m11) < equalsEpsilon;
	    boolean m12Equal = Math.abs(this.m12 - matrix.m12) < equalsEpsilon;
	    boolean m13Equal = Math.abs(this.m13 - matrix.m13) < equalsEpsilon;
	    boolean m14Equal = Math.abs(this.m14 - matrix.m14) < equalsEpsilon;
	    boolean m1Equal = m11Equal && m12Equal && m13Equal && m14Equal;
	    
	    boolean m21Equal = Math.abs(this.m21 - matrix.m21) < equalsEpsilon;
	    boolean m22Equal = Math.abs(this.m22 - matrix.m22) < equalsEpsilon;
	    boolean m23Equal = Math.abs(this.m23 - matrix.m23) < equalsEpsilon;
	    boolean m24Equal = Math.abs(this.m24 - matrix.m24) < equalsEpsilon;
	    boolean m2Equal = m21Equal && m22Equal && m23Equal && m24Equal;
	    
	    boolean m31Equal = Math.abs(this.m31 - matrix.m31) < equalsEpsilon;
	    boolean m32Equal = Math.abs(this.m32 - matrix.m32) < equalsEpsilon;
	    boolean m33Equal = Math.abs(this.m33 - matrix.m33) < equalsEpsilon;
	    boolean m34Equal = Math.abs(this.m34 - matrix.m34) < equalsEpsilon;
	    boolean m3Equal = m31Equal && m32Equal && m33Equal && m34Equal;
	    
	    boolean m41Equal = Math.abs(this.m41 - matrix.m41) < equalsEpsilon;
	    boolean m42Equal = Math.abs(this.m42 - matrix.m42) < equalsEpsilon;
	    boolean m43Equal = Math.abs(this.m43 - matrix.m43) < equalsEpsilon;
	    boolean m44Equal = Math.abs(this.m44 - matrix.m44) < equalsEpsilon;
	    boolean m4Equal = m41Equal && m42Equal && m43Equal && m44Equal;
	    
	    return m1Equal && m2Equal && m3Equal && m4Equal;
	}
	
	public Matrix4 copy() {
		return new Matrix4(this);
	}
	
	public Matrix4 set(Matrix4 matrix) {
		this.m11 = matrix.m11;
		this.m12 = matrix.m12;
		this.m13 = matrix.m13;
		this.m14 = matrix.m14;
		
		this.m21 = matrix.m21;
		this.m22 = matrix.m22;
		this.m23 = matrix.m23;
		this.m24 = matrix.m24;
		
		this.m31 = matrix.m31;
		this.m32 = matrix.m32;
		this.m33 = matrix.m33;
		this.m34 = matrix.m34;
		
		this.m41 = matrix.m41;
		this.m42 = matrix.m42;
		this.m43 = matrix.m43;
		this.m44 = matrix.m44;
		
		return this;
	}

	public Matrix4 add(Matrix4 matrix) {
		Matrix4 output = new Matrix4();
		
		output.m11 = this.m11 + matrix.m11;
		output.m12 = this.m12 + matrix.m12;
		output.m13 = this.m13 + matrix.m13;
		output.m14 = this.m14 + matrix.m14;
		
		output.m21 = this.m21 + matrix.m21;
		output.m22 = this.m22 + matrix.m22;
		output.m23 = this.m23 + matrix.m23;
		output.m24 = this.m24 + matrix.m24;
		
		output.m31 = this.m31 + matrix.m31;
		output.m32 = this.m32 + matrix.m32;
		output.m33 = this.m33 + matrix.m33;
		output.m34 = this.m34 + matrix.m34;
		
		output.m41 = this.m41 + matrix.m41;
		output.m42 = this.m42 + matrix.m42;
		output.m43 = this.m43 + matrix.m43;
		output.m44 = this.m44 + matrix.m44;
		
		return output;
	}

	public Matrix4 subtract(Matrix4 matrix) {
		Matrix4 output = new Matrix4();
		
		output.m11 = this.m11 - matrix.m11;
		output.m12 = this.m12 - matrix.m12;
		output.m13 = this.m13 - matrix.m13;
		output.m14 = this.m14 - matrix.m14;
		
		output.m21 = this.m21 - matrix.m21;
		output.m22 = this.m22 - matrix.m22;
		output.m23 = this.m23 - matrix.m23;
		output.m24 = this.m24 - matrix.m24;
		
		output.m31 = this.m31 - matrix.m31;
		output.m32 = this.m32 - matrix.m32;
		output.m33 = this.m33 - matrix.m33;
		output.m34 = this.m34 - matrix.m34;
		
		output.m41 = this.m41 - matrix.m41;
		output.m42 = this.m42 - matrix.m42;
		output.m43 = this.m43 - matrix.m43;
		output.m44 = this.m44 - matrix.m44;
		
		return output;
	}

	public Matrix4 multiply(Matrix4 matrix) {
		Matrix4 output = new Matrix4();
		
		output.m11 = this.m11 * matrix.m11 + this.m12 * matrix.m21 + this.m13 * matrix.m31 + this.m14 * matrix.m41;
		output.m12 = this.m11 * matrix.m12 + this.m12 * matrix.m22 + this.m13 * matrix.m32 + this.m14 * matrix.m42;
		output.m13 = this.m11 * matrix.m13 + this.m12 * matrix.m23 + this.m13 * matrix.m33 + this.m14 * matrix.m43;
		output.m14 = this.m11 * matrix.m14 + this.m12 * matrix.m24 + this.m13 * matrix.m34 + this.m14 * matrix.m44;
		output.m21 = this.m21 * matrix.m11 + this.m22 * matrix.m21 + this.m23 * matrix.m31 + this.m24 * matrix.m41;
		output.m22 = this.m21 * matrix.m12 + this.m22 * matrix.m22 + this.m23 * matrix.m32 + this.m24 * matrix.m42;
		output.m23 = this.m21 * matrix.m13 + this.m22 * matrix.m23 + this.m23 * matrix.m33 + this.m24 * matrix.m43;
		output.m24 = this.m21 * matrix.m14 + this.m22 * matrix.m24 + this.m23 * matrix.m34 + this.m24 * matrix.m44;
		output.m31 = this.m31 * matrix.m11 + this.m32 * matrix.m21 + this.m33 * matrix.m31 + this.m34 * matrix.m41;
		output.m32 = this.m31 * matrix.m12 + this.m32 * matrix.m22 + this.m33 * matrix.m32 + this.m34 * matrix.m42;
		output.m33 = this.m31 * matrix.m13 + this.m32 * matrix.m23 + this.m33 * matrix.m33 + this.m34 * matrix.m43;
		output.m34 = this.m31 * matrix.m14 + this.m32 * matrix.m24 + this.m33 * matrix.m34 + this.m34 * matrix.m44;
		output.m41 = this.m41 * matrix.m11 + this.m42 * matrix.m21 + this.m43 * matrix.m31 + this.m44 * matrix.m41;
		output.m42 = this.m41 * matrix.m12 + this.m42 * matrix.m22 + this.m43 * matrix.m32 + this.m44 * matrix.m42;
		output.m43 = this.m41 * matrix.m13 + this.m42 * matrix.m23 + this.m43 * matrix.m33 + this.m44 * matrix.m43;
		output.m44 = this.m41 * matrix.m14 + this.m42 * matrix.m24 + this.m43 * matrix.m34 + this.m44 * matrix.m44;

		return output;
	}
	
	public Matrix4 transpose() {
		Matrix4 output = new Matrix4();
		
		output.m11 = this.m11;
		output.m12 = this.m21;
		output.m13 = this.m31;
		output.m14 = this.m41;
		
		output.m21 = this.m12;
		output.m22 = this.m22;
		output.m23 = this.m32;
		output.m24 = this.m42;
		
		output.m31 = this.m13;
		output.m32 = this.m23;
		output.m33 = this.m33;
		output.m34 = this.m43;
		
		output.m41 = this.m14;
		output.m42 = this.m24;
		output.m43 = this.m34;
		output.m44 = this.m44;
		
		return output;
	}
	
	public Matrix4 inverse() {
		Matrix4 output = new Matrix4();
		
		double s0 = this.m11 * this.m22 - this.m21 * this.m12;
		double s1 = this.m11 * this.m23 - this.m21 * this.m13;
		double s2 = this.m11 * this.m24 - this.m21 * this.m14;
		double s3 = this.m12 * this.m23 - this.m22 * this.m13;
		double s4 = this.m12 * this.m24 - this.m22 * this.m14;
		double s5 = this.m13 * this.m24 - this.m23 * this.m14;
		
		double c5 = this.m33 * this.m44 - this.m43 * this.m34;
		double c4 = this.m32 * this.m44 - this.m42 * this.m34;
		double c3 = this.m32 * this.m43 - this.m42 * this.m33;
		double c2 = this.m31 * this.m44 - this.m41 * this.m34;
		double c1 = this.m31 * this.m43 - this.m41 * this.m33;
		double c0 = this.m31 * this.m42 - this.m41 * this.m32;
		
		double inverseDeterminant = 1.0 / (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);
		
		output.m11 = ( this.m22 * c5 - this.m23 * c4 + this.m24 * c3) * inverseDeterminant;
		output.m12 = (-this.m12 * c5 + this.m13 * c4 - this.m14 * c3) * inverseDeterminant;
		output.m13 = ( this.m42 * s5 - this.m43 * s4 + this.m44 * s3) * inverseDeterminant;
		output.m14 = (-this.m32 * s5 + this.m33 * s4 - this.m34 * s3) * inverseDeterminant;
		
		output.m21 = (-this.m21 * c5 + this.m23 * c2 - this.m24 * c1) * inverseDeterminant;
		output.m22 = ( this.m11 * c5 - this.m13 * c2 + this.m14 * c1) * inverseDeterminant;
		output.m23 = (-this.m41 * s5 + this.m43 * s2 - this.m44 * s1) * inverseDeterminant;
		output.m24 = ( this.m31 * s5 - this.m33 * s2 + this.m34 * s1) * inverseDeterminant;
		
		output.m31 = ( this.m21 * c4 - this.m22 * c2 + this.m24 * c0) * inverseDeterminant;
		output.m32 = (-this.m11 * c4 + this.m12 * c2 - this.m14 * c0) * inverseDeterminant;
		output.m33 = ( this.m41 * s4 - this.m42 * s2 + this.m44 * s0) * inverseDeterminant;
		output.m34 = (-this.m31 * s4 + this.m32 * s2 - this.m34 * s0) * inverseDeterminant;
		
		output.m41 = (-this.m21 * c3 + this.m22 * c1 - this.m23 * c0) * inverseDeterminant;
		output.m42 = ( this.m11 * c3 - this.m12 * c1 + this.m13 * c0) * inverseDeterminant;
		output.m43 = (-this.m41 * s3 + this.m42 * s1 - this.m43 * s0) * inverseDeterminant;
		output.m44 = ( this.m31 * s3 - this.m32 * s1 + this.m33 * s0) * inverseDeterminant;
		
		return output;
	}
}
