package uk.co.connieprice.javasoftwarerenderer.math;

/**
 * <h1>Vector3</h1>
 * A helper class to store 3d positions.
 *
 * @author Connie Price
 *
 */
public class Vector3 implements Vector<Vector3> {
	public final static Vector3 X = new Vector3(1, 0, 0);
	public final static Vector3 Y = new Vector3(0, 1, 0);
	public final static Vector3 Z = new Vector3(0, 0, 1);
	public final static Vector3 ZERO = new Vector3(0, 0, 0);

	public double x;
	public double y;
	public double z;

	public Vector3() {}
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public Vector3(Vector3 vector) {
		this.set(vector);
	}
	
	public String toString() {
        return String.format("Vector3(%f, %f, %f)", this.x, this.y, this.z);
    }
	
	private static double equalsEpsilon = 0.00001;
	@Override
	public boolean equals(Object object) {
	    if (this == object) return true;
	    if (object == null || getClass() != object.getClass()) return false;

	    Vector3 vector = (Vector3) object;
	    return Math.abs(this.x - vector.x) < equalsEpsilon && Math.abs(this.y - vector.y) < equalsEpsilon && Math.abs(this.z - vector.z) < equalsEpsilon;
	}
	
	@Override
	public Vector3 copy() {
		return new Vector3(this);
	}
	
	@Override
	public double length() {
		return Math.sqrt(this.squareLength());
	}
	
	@Override
	public double squareLength() {
		return x * x + y * y + z * z;
	}
	
	@Override
	public Vector3 setLength(double length) {
		return this.setSquareLength(length * length);
	}
	
	@Override
	public Vector3 setSquareLength(double squareLength) {
		double oldSquareLength = this.squareLength();
		return (oldSquareLength == 0 || oldSquareLength == squareLength) ? this : this.multiply(Math.sqrt(squareLength / oldSquareLength));
	}
	
	@Override
	public Vector3 set(Vector3 vector) {
		this.x = vector.x;
		this.y = vector.y;
		this.z = vector.z;
		return this;
	}
	
	public Vector3 set(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}
	
	@Override
	public Vector3 setZero() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		return this;
	}
	
	@Override
	public Vector3 add(Vector3 vector) {
		this.x += vector.x;
		this.y += vector.y;
		this.z += vector.z;
		return this;
	}
	
	public Vector3 add(int i) {
		this.x += i;
		this.y += i;
		this.z += i;
		return this;
	}
	
	@Override
	public Vector3 subtract(Vector3 vector) {
		this.x -= vector.x;
		this.y -= vector.y;
		this.z -= vector.z;
		return this;
	}
	
	public Vector3 subtract(int i) {
		this.x -= i;
		this.y -= i;
		this.z -= i;
		return this;
	}
	
	@Override
	public Vector3 normalize() {
		double length = this.length();
		if (length != 0) {
			this.x /= length;
			this.y /= length;
			this.z /= length;
		}
		return this;
	}
	
	@Override
	public Vector3 multiply(double value) {
		this.x *= value;
		this.y *= value;
		this.z *= value;
		return this;
	}
	
	@Override
	public Vector3 multiply(Vector3 vector) {
		this.x *= vector.x;
		this.y *= vector.y;
		this.z *= vector.z;
		return this;
	}
	
	@Override
	public Vector3 multiply(Matrix matrix) {
		double newX = x * matrix.matrixArray[0][0] + y * matrix.matrixArray[0][1] + z * matrix.matrixArray[0][2] + matrix.matrixArray[0][3];
		double newY = x * matrix.matrixArray[1][0] + y * matrix.matrixArray[1][1] + z * matrix.matrixArray[1][2] + matrix.matrixArray[1][3];
		double newZ = x * matrix.matrixArray[2][0] + y * matrix.matrixArray[2][1] + z * matrix.matrixArray[2][2] + matrix.matrixArray[2][3];
		
		this.x = newX;
		this.y = newY;
		this.z = newZ;
		
		return this;
	}
	
	@Override
	public Vector3 multiply(Matrix4 matrix) {
		this.x = x * matrix.m11 + y * matrix.m12 + z * matrix.m13 + matrix.m14;
		this.y = x * matrix.m21 + y * matrix.m22 + z * matrix.m23 + matrix.m24;
		this.z = x * matrix.m31 + y * matrix.m32 + z * matrix.m33 + matrix.m34;
		
		return this;
	}
	
	@Override
	public Vector3 invert() {
		return this.multiply(-1);
	}
	
	@Override
	public Vector3 divide(double value) {
		this.x /= value;
		this.y /= value;
		this.z /= value;
		return this;
	}
	
	@Override
	public Vector3 divide(Vector3 vector) {
		this.x /= vector.x;
		this.y /= vector.y;
		this.z /= vector.z;
		return this;
	}
	
	@Override
	public Vector3 power(double value) {
		this.x = Math.pow(this.x, value);
		this.y = Math.pow(this.y, value);
		this.z = Math.pow(this.z, value);
		return this;
	}
	
	@Override
	public Vector3 lerp(Vector3 vector, double value) {
		this.x = this.x + (vector.x - this.x) * value;
		this.y = this.y + (vector.y - this.y) * value;
		this.z = this.z + (vector.z - this.z) * value;
		return this;
	}
	
	@Override
	public Vector3 vectorDistance(Vector3 vector) {
		Vector3 output = new Vector3();
		output.x = vector.x - this.x;
		output.y = vector.y - this.y;
		output.z = vector.z - this.z;
		return output;
	}
	
	@Override
	public double distance(Vector3 vector) {
		return Math.sqrt(this.squareDistance(vector));
	}
	
	@Override
	public double squareDistance(Vector3 vector) {
		final double deltaX = vector.x - this.x;
		final double deltaY = vector.y - this.y;
		final double deltaZ = vector.z - this.z;
		return deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ;
	}
	
	public Vector3 crossProduct(Vector3 vector) {
		double x = (this.y * vector.z) - (this.z * vector.y);
		double y = (this.z * vector.x) - (this.x * vector.z);
		double z = (this.x * vector.y) - (this.y * vector.x);
		
		this.x = x;
		this.y = y;
		this.z = z;
		
		return this;
	}
	
	@Override
	public double dotProduct(Vector3 vector) {
		return (this.x * vector.x) + (this.y * vector.y) + (this.z * vector.z);
	}
}
