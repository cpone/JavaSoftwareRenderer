package uk.co.connieprice.javasoftwarerenderer.math;

/**
 * <h1>Euler</h1>
 * A helper class to store euler rotations.
 *
 * @author Connie Price
 *
 */
public class Euler {
	public double pitch; // X Axis
	public double yaw; // Y Axis
	public double roll; // Z Axis

	public Euler() {}
	public Euler(double pitch, double yaw, double roll) {
		this.pitch = pitch;
		this.yaw = yaw;
		this.roll = roll;
	}
	public Euler(Euler euler) {
		this.set(euler);
	}

	public Euler copy() {
		return new Euler(this);
	}
	
	public Euler set(Euler euler) {
		this.pitch = euler.pitch;
		this.yaw = euler.yaw;
		this.roll = euler.roll;
		return this;
	}
	
	public Euler invert() {
		this.pitch = -this.pitch;
		this.yaw = -this.yaw;
		this.roll = -this.roll;
		return this;
	}
	
	public Vector3 forward() {
		double cosPitch = Math.cos(pitch);
		double cosYaw = Math.cos(yaw);
		
		double sinPitch = Math.sin(pitch);
		double sinYaw = Math.sin(yaw);
		
		return new Vector3(
				sinYaw,
				sinPitch * cosYaw * -1,
				cosPitch * cosYaw
		);
	}
}
