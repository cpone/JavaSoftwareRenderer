package uk.co.connieprice.javasoftwarerenderer.math;

public interface Vector<Type extends Vector<Type>> {
	Type copy();
	
	double length();
	double squareLength();
	Type setLength(double limit);
	Type setSquareLength(double limit);
	
	Type set(Type vector);
	Type setZero();
	
	Type add(Type vector);
	Type subtract(Type vector);
	Type normalize();
	
	Type multiply(double value);
	Type multiply(Type vector);
	Type multiply(Matrix matrix);
	Type multiply(Matrix4 matrix);
	
	Type invert();
	
	Type divide(double value);
	Type divide(Type vector);
	
	Type power(double value);
	
	Type lerp(Type vector, double value);
	
	Type vectorDistance(Type vector);
	
	double distance(Type vector);
	double squareDistance(Type vector);
	
	double dotProduct(Type vector);
}
