package uk.co.connieprice.javasoftwarerenderer.math;

/**
 * <h1>Vector2</h1>
 * A helper class to store 2d positions.
 *
 * @author Connie Price
 *
 */
public class Vector2 implements Vector<Vector2> {
	public final static Vector2 X = new Vector2(1, 0);
	public final static Vector2 Y = new Vector2(0, 1);
	public final static Vector2 ZERO = new Vector2(0, 0);

	public double x;
	public double y;

	public Vector2() {}
	public Vector2(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public Vector2(Vector2 vector) {
		this.set(vector);
	}
	
	@Override
	public boolean equals(Object object) {
	    if (this == object) return true;
	    if (object == null || getClass() != object.getClass()) return false;

	    Vector2 vector = (Vector2) object;
	    return this.x == vector.x && this.y == vector.y;
	}
	
	@Override
	public Vector2 copy() {
		return new Vector2(this);
	}
	
	@Override
	public double length() {
		return Math.sqrt(this.squareLength());
	}
	
	@Override
	public double squareLength() {
		return x * x + y * y;
	}
	
	@Override
	public Vector2 setLength(double length) {
		return this.setSquareLength(length * length);
	}
	
	@Override
	public Vector2 setSquareLength(double squareLength) {
		double oldSquareLength = this.squareLength();
		return (oldSquareLength == 0 || oldSquareLength == squareLength) ? this : this.multiply(Math.sqrt(squareLength / oldSquareLength));
	}
	
	@Override
	public Vector2 set(Vector2 vector) {
		this.x = vector.x;
		this.y = vector.y;
		return this;
	}
	
	@Override
	public Vector2 setZero() {
		this.x = 0;
		this.y = 0;
		return this;
	}
	
	@Override
	public Vector2 add(Vector2 vector) {
		this.x += vector.x;
		this.y += vector.y;
		return this;
	}
	
	@Override
	public Vector2 subtract(Vector2 vector) {
		this.x -= vector.x;
		this.y -= vector.y;
		return this;
	}
	
	@Override
	public Vector2 normalize() {
		double length = this.length();
		if (length != 0) {
			this.x /= length;
			this.y /= length;
		}
		return this;
	}
	
	@Override
	public Vector2 multiply(double value) {
		this.x *= value;
		this.y *= value;
		return this;
	}
	
	@Override
	public Vector2 multiply(Vector2 vector) {
		this.x *= vector.x;
		this.y *= vector.y;
		return this;
	}
	
	@Override
	public Vector2 multiply(Matrix matrix) {
		Matrix vector = new Matrix(4, 1);
		vector.setElement(0, 0, this.x);
		vector.setElement(1, 0, this.y);
		vector.setElement(2, 0, 1);
		
		Matrix output = matrix.multiply(vector);
		this.x = output.getElement(0, 0);
		this.y = output.getElement(1, 0);
		
		return this;
	}
	
	@Override
	public Vector2 multiply(Matrix4 matrix) {
		this.x = x * matrix.m11 + y * matrix.m12 + matrix.m13;
		this.y = x * matrix.m21 + y * matrix.m22 + matrix.m23;
		
		return this;
	}
	
	@Override
	public Vector2 invert() {
		return this.multiply(-1);
	}
	
	@Override
	public Vector2 divide(double value) {
		this.x /= value;
		this.y /= value;
		return this;
	}
	
	@Override
	public Vector2 divide(Vector2 vector) {
		this.x /= vector.x;
		this.y /= vector.y;
		return this;
	}
	
	@Override
	public Vector2 power(double value) {
		this.x = Math.pow(this.x, value);
		this.y = Math.pow(this.y, value);
		return this;
	}
	
	@Override
	public Vector2 lerp(Vector2 vector, double value) {
		this.x = this.x + (vector.x - this.x) * value;
		this.y = this.y + (vector.y - this.y) * value;
		return this;
	}
	
	@Override
	public Vector2 vectorDistance(Vector2 vector) {
		Vector2 output = new Vector2();
		output.x = vector.x - this.x;
		output.y = vector.y - this.y;
		return output;
	}
	
	@Override
	public double distance(Vector2 vector) {
		return Math.sqrt(this.squareDistance(vector));
	}
	
	@Override
	public double squareDistance(Vector2 vector) {
		final double deltaX = vector.x - this.x;
		final double deltaY = vector.y - this.y;
		return deltaX * deltaX + deltaY * deltaY;
	}
	
	@Override
	public double dotProduct(Vector2 vector) {
		return (this.x * vector.x) + (this.y * vector.y);
	}
}
