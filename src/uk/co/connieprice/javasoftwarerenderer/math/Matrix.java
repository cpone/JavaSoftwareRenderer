package uk.co.connieprice.javasoftwarerenderer.math;


// TODO Make Matrix4 because hardcoding is faster sometimes.
public class Matrix {
	public static final Matrix TRANSFORMLESS = new Matrix(new double[][] {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
	});

	public static Matrix FromTranslation(Vector3 translation) {
		return new Matrix(new double[][] {
			{1, 0, 0, translation.x},
			{0, 1, 0, translation.y},
			{0, 0, 1, translation.z},
			{0, 0, 0,             1}
		});
	}

	// Expanded Euler matrices so that no matrix multiplication is required to take place.
	public static Matrix FromEuler(Euler rotation) {
		double pitch = rotation.pitch;
		double yaw = rotation.yaw;
		double roll = rotation.roll;

		double cosPitch = Math.cos(pitch);
		double cosYaw = Math.cos(yaw);
		double cosRoll = Math.cos(roll);
		
		double sinPitch = Math.sin(pitch);
		double sinYaw = Math.sin(yaw);
		double sinRoll = Math.sin(roll);
		
		double m11 = cosYaw * cosRoll;
		double m12 = cosYaw * sinRoll * -1;
		double m13 = sinYaw;
		double m14 = 0;

		double m21 = (sinPitch * sinYaw * cosRoll) + (cosPitch * sinRoll);
		double m22 = (sinPitch * sinYaw * sinRoll) + (cosPitch * cosRoll);
		double m23 = sinPitch * cosYaw * -1;
		double m24 = 0;

		double m31 = (cosPitch * sinYaw * cosRoll * -1) + (sinPitch * sinRoll);
		double m32 = (cosPitch * sinYaw * sinRoll)      + (sinPitch * cosRoll);
		double m33 = cosPitch * cosYaw;
		double m34 = 0;

		double m41 = 0;
		double m42 = 0;
		double m43 = 0;
		double m44 = 1;

		Matrix rotationMatrix = new Matrix(new double[][] {
			{m11, m12, m13, m14},
			{m21, m22, m23, m24},
			{m31, m32, m33, m34},
			{m41, m42, m43, m44},
		});
		
		return rotationMatrix;
	}

	public static Matrix FromScale(Vector3 scale) {
		return new Matrix(new double[][] {
			{scale.x,       0,       0, 0},
			{      0, scale.y,       0, 0},
			{      0,       0, scale.z, 0},
			{      0,       0,       0, 1}
		});
	}

	public static Matrix OrthographicProjection(double left, double right, double bottom, double top, double near, double far) {
		double m11 = 2 / (right - left);
		double m12 = 0;
		double m13 = 0;
		double m14 = (right + left) / (right - left);

		double m21 = 0;
		double m22 = 2 / (top - bottom);
		double m23 = 0;
		double m24 = (top + bottom) / (top - bottom);

		double m31 = 0;
		double m32 = 0;
		double m33 = 2 / (near + far);
		double m34 = (near - far) / (near + far);

		double m41 = 0;
		double m42 = 0;
		double m43 = 0;
		double m44 = 1;

		Matrix orthographicMatrix = new Matrix(new double[][] {
			{m11, m12, m13, m14},
			{m21, m22, m23, m24},
			{m31, m32, m33, m34},
			{m41, m42, m43, m44},
		});
		
		return orthographicMatrix;
	}
	
	public static Matrix PerspectiveProjection(double horizontalFOV, double verticalFOV, double near, double far) {
		return new Matrix(new double[][] {
			{1, 0, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		});
	}

	public static Matrix ProjectionToScreen(double width, double height, double depth) {
		return new Matrix(new double[][] {
			{width/2,        0,       0, width},
			{      0, height/2,       0, height},
			{      0,        0, depth/2, depth},
			{      0,        0,       0, 1}
		});
	}

	public static Matrix ScreenToProjection(double width, double height, double depth) {
		return new Matrix(new double[][] {
			{2/width,        0,       0, -1},
			{      0, 2/height,       0, -1},
			{      0,        0, 2/depth, -1},
			{      0,        0,       0, 1}
		});
	}
	
	public double[][] matrixArray;

	public int rows;
	public int columns;

	public Matrix(double[][] newMatrixArray) {
		this.rows = newMatrixArray.length;
		this.columns = newMatrixArray[0].length;
		
		this.matrixArray = newMatrixArray;
	}
	
	public Matrix(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		
		this.matrixArray = new double[this.rows][this.columns];

		for(int row = 0; row < this.rows; row++) {
			for(int column = 0; column < this.columns; column++) {
				this.matrixArray[row][column] = 0;
			}
		}
	}

	public Matrix(Matrix matrix) {
		this.set(matrix);
	}
	
	public Matrix copy() {
		return new Matrix(this);
	}
	
	public Matrix set(Matrix matrix) {
		this.rows = matrix.rows;
		this.columns = matrix.columns;
		
		this.matrixArray = new double[this.rows][this.columns];

		for(int row = 0; row < this.rows; row++) {
			for(int column = 0; column < this.columns; column++) {
				this.matrixArray[row][column] = matrix.matrixArray[row][column];
			}
		}
		
		return this;
	}
	
	public double getElement(int row, int column) {
		return this.matrixArray[row][column];
	}
	
	public Matrix setElement(int row, int column, double value) {
		this.matrixArray[row][column] = value;
		return this;
	}
	
	public Matrix add(Matrix matrix) {
		Matrix output = new Matrix(this.rows, matrix.columns);
		
		for(int row = 0; row < this.rows; row++) {
			for(int column = 0; column < matrix.columns; column++) {
	        	output.matrixArray[row][column] = this.matrixArray[row][column] + matrix.matrixArray[row][column];
	        }
	    }
		
		return output;
	}

	public Matrix subtract(Matrix matrix) {
		Matrix output = new Matrix(this.rows, matrix.columns);
		
		for(int row = 0; row < this.rows; row++) {
			for(int column = 0; column < matrix.columns; column++) {
	        	output.matrixArray[row][column] = this.matrixArray[row][column] - matrix.matrixArray[row][column];
	        }
	    }
		
		return output;
	}

	public Matrix multiply(Matrix matrix) {
		Matrix output = new Matrix(this.rows, matrix.columns);
		
		output.matrixArray[0][0] = this.matrixArray[0][0] * matrix.matrixArray[0][0] + this.matrixArray[0][1] * matrix.matrixArray[1][0] + this.matrixArray[0][2] * matrix.matrixArray[2][0] + this.matrixArray[0][3] * matrix.matrixArray[3][0];
		output.matrixArray[0][1] = this.matrixArray[0][0] * matrix.matrixArray[0][1] + this.matrixArray[0][1] * matrix.matrixArray[1][1] + this.matrixArray[0][2] * matrix.matrixArray[2][1] + this.matrixArray[0][3] * matrix.matrixArray[3][1];
		output.matrixArray[0][2] = this.matrixArray[0][0] * matrix.matrixArray[0][2] + this.matrixArray[0][1] * matrix.matrixArray[1][2] + this.matrixArray[0][2] * matrix.matrixArray[2][2] + this.matrixArray[0][3] * matrix.matrixArray[3][2];
		output.matrixArray[0][3] = this.matrixArray[0][0] * matrix.matrixArray[0][3] + this.matrixArray[0][1] * matrix.matrixArray[1][3] + this.matrixArray[0][2] * matrix.matrixArray[2][3] + this.matrixArray[0][3] * matrix.matrixArray[3][3];
		output.matrixArray[1][0] = this.matrixArray[1][0] * matrix.matrixArray[0][0] + this.matrixArray[1][1] * matrix.matrixArray[1][0] + this.matrixArray[1][2] * matrix.matrixArray[2][0] + this.matrixArray[1][3] * matrix.matrixArray[3][0];
		output.matrixArray[1][1] = this.matrixArray[1][0] * matrix.matrixArray[0][1] + this.matrixArray[1][1] * matrix.matrixArray[1][1] + this.matrixArray[1][2] * matrix.matrixArray[2][1] + this.matrixArray[1][3] * matrix.matrixArray[3][1];
		output.matrixArray[1][2] = this.matrixArray[1][0] * matrix.matrixArray[0][2] + this.matrixArray[1][1] * matrix.matrixArray[1][2] + this.matrixArray[1][2] * matrix.matrixArray[2][2] + this.matrixArray[1][3] * matrix.matrixArray[3][2];
		output.matrixArray[1][3] = this.matrixArray[1][0] * matrix.matrixArray[0][3] + this.matrixArray[1][1] * matrix.matrixArray[1][3] + this.matrixArray[1][2] * matrix.matrixArray[2][3] + this.matrixArray[1][3] * matrix.matrixArray[3][3];
		output.matrixArray[2][0] = this.matrixArray[2][0] * matrix.matrixArray[0][0] + this.matrixArray[2][1] * matrix.matrixArray[1][0] + this.matrixArray[2][2] * matrix.matrixArray[2][0] + this.matrixArray[2][3] * matrix.matrixArray[3][0];
		output.matrixArray[2][1] = this.matrixArray[2][0] * matrix.matrixArray[0][1] + this.matrixArray[2][1] * matrix.matrixArray[1][1] + this.matrixArray[2][2] * matrix.matrixArray[2][1] + this.matrixArray[2][3] * matrix.matrixArray[3][1];
		output.matrixArray[2][2] = this.matrixArray[2][0] * matrix.matrixArray[0][2] + this.matrixArray[2][1] * matrix.matrixArray[1][2] + this.matrixArray[2][2] * matrix.matrixArray[2][2] + this.matrixArray[2][3] * matrix.matrixArray[3][2];
		output.matrixArray[2][3] = this.matrixArray[2][0] * matrix.matrixArray[0][3] + this.matrixArray[2][1] * matrix.matrixArray[1][3] + this.matrixArray[2][2] * matrix.matrixArray[2][3] + this.matrixArray[2][3] * matrix.matrixArray[3][3];
		output.matrixArray[3][0] = this.matrixArray[3][0] * matrix.matrixArray[0][0] + this.matrixArray[3][1] * matrix.matrixArray[1][0] + this.matrixArray[3][2] * matrix.matrixArray[2][0] + this.matrixArray[3][3] * matrix.matrixArray[3][0];
		output.matrixArray[3][1] = this.matrixArray[3][0] * matrix.matrixArray[0][1] + this.matrixArray[3][1] * matrix.matrixArray[1][1] + this.matrixArray[3][2] * matrix.matrixArray[2][1] + this.matrixArray[3][3] * matrix.matrixArray[3][1];
		output.matrixArray[3][2] = this.matrixArray[3][0] * matrix.matrixArray[0][2] + this.matrixArray[3][1] * matrix.matrixArray[1][2] + this.matrixArray[3][2] * matrix.matrixArray[2][2] + this.matrixArray[3][3] * matrix.matrixArray[3][2];
		output.matrixArray[3][3] = this.matrixArray[3][0] * matrix.matrixArray[0][3] + this.matrixArray[3][1] * matrix.matrixArray[1][3] + this.matrixArray[3][2] * matrix.matrixArray[2][3] + this.matrixArray[3][3] * matrix.matrixArray[3][3];

		return output;
	}
	
	public Matrix transpose() {
		Matrix output = new Matrix(this.columns, this.rows);

		for(int row = 0; row < this.rows; row++) {
			for(int column = 0; column < this.columns; column++) {
				output.matrixArray[column][row] = this.matrixArray[row][column];
			}
		}

		return output;
	}
	
	// Assumed 4x4
	public double determinant() {
		return    this.matrixArray[3][0] * this.matrixArray[2][1] * this.matrixArray[1][2] * this.matrixArray[0][3] - this.matrixArray[2][0] * this.matrixArray[3][1] * this.matrixArray[1][2] * this.matrixArray[0][3]
				- this.matrixArray[3][0] * this.matrixArray[1][1] * this.matrixArray[2][2] * this.matrixArray[0][3] + this.matrixArray[1][0] * this.matrixArray[3][1] * this.matrixArray[2][2] * this.matrixArray[0][3]
				+ this.matrixArray[2][0] * this.matrixArray[1][1] * this.matrixArray[3][2] * this.matrixArray[0][3] - this.matrixArray[1][0] * this.matrixArray[2][1] * this.matrixArray[3][2] * this.matrixArray[0][3]
				- this.matrixArray[3][0] * this.matrixArray[2][1] * this.matrixArray[0][2] * this.matrixArray[1][3] + this.matrixArray[2][0] * this.matrixArray[3][1] * this.matrixArray[0][2] * this.matrixArray[1][3]
				+ this.matrixArray[3][0] * this.matrixArray[0][1] * this.matrixArray[2][2] * this.matrixArray[1][3] - this.matrixArray[0][0] * this.matrixArray[3][1] * this.matrixArray[2][2] * this.matrixArray[1][3]
				- this.matrixArray[2][0] * this.matrixArray[0][1] * this.matrixArray[3][2] * this.matrixArray[1][3] + this.matrixArray[0][0] * this.matrixArray[2][1] * this.matrixArray[3][2] * this.matrixArray[1][3]
				+ this.matrixArray[3][0] * this.matrixArray[1][1] * this.matrixArray[0][2] * this.matrixArray[2][3] - this.matrixArray[1][0] * this.matrixArray[3][1] * this.matrixArray[0][2] * this.matrixArray[2][3]
				- this.matrixArray[3][0] * this.matrixArray[0][1] * this.matrixArray[1][2] * this.matrixArray[2][3] + this.matrixArray[0][0] * this.matrixArray[3][1] * this.matrixArray[1][2] * this.matrixArray[2][3]
				+ this.matrixArray[1][0] * this.matrixArray[0][1] * this.matrixArray[3][2] * this.matrixArray[2][3] - this.matrixArray[0][0] * this.matrixArray[1][1] * this.matrixArray[3][2] * this.matrixArray[2][3]
				- this.matrixArray[2][0] * this.matrixArray[1][1] * this.matrixArray[0][2] * this.matrixArray[3][3] + this.matrixArray[1][0] * this.matrixArray[2][1] * this.matrixArray[0][2] * this.matrixArray[3][3]
				+ this.matrixArray[2][0] * this.matrixArray[0][1] * this.matrixArray[1][2] * this.matrixArray[3][3] - this.matrixArray[0][0] * this.matrixArray[2][1] * this.matrixArray[1][2] * this.matrixArray[3][3]
				- this.matrixArray[1][0] * this.matrixArray[0][1] * this.matrixArray[2][2] * this.matrixArray[3][3] + this.matrixArray[0][0] * this.matrixArray[1][1] * this.matrixArray[2][2] * this.matrixArray[3][3];
	}
	
	// Assumed 4x4
	public Matrix inverse() {		
		double determinant = this.determinant();
		if (determinant == 0) return this.copy();
		
		Matrix output = new Matrix(this.columns, this.rows);
		
		double m00 = this.matrixArray[1][2] * this.matrixArray[2][3] * this.matrixArray[3][1] - this.matrixArray[1][3] * this.matrixArray[2][2] * this.matrixArray[3][1]
		           + this.matrixArray[1][3] * this.matrixArray[2][1] * this.matrixArray[3][2] - this.matrixArray[1][1] * this.matrixArray[2][3] * this.matrixArray[3][2]
		           - this.matrixArray[1][2] * this.matrixArray[2][1] * this.matrixArray[3][3] + this.matrixArray[1][1] * this.matrixArray[2][2] * this.matrixArray[3][3];
		double m01 = this.matrixArray[0][3] * this.matrixArray[2][2] * this.matrixArray[3][1] - this.matrixArray[0][2] * this.matrixArray[2][3] * this.matrixArray[3][1]
		           - this.matrixArray[0][3] * this.matrixArray[2][1] * this.matrixArray[3][2] + this.matrixArray[0][1] * this.matrixArray[2][3] * this.matrixArray[3][2]
		           + this.matrixArray[0][2] * this.matrixArray[2][1] * this.matrixArray[3][3] - this.matrixArray[0][1] * this.matrixArray[2][2] * this.matrixArray[3][3];
		double m02 = this.matrixArray[0][2] * this.matrixArray[1][3] * this.matrixArray[3][1] - this.matrixArray[0][3] * this.matrixArray[1][2] * this.matrixArray[3][1]
		           + this.matrixArray[0][3] * this.matrixArray[1][1] * this.matrixArray[3][2] - this.matrixArray[0][1] * this.matrixArray[1][3] * this.matrixArray[3][2]
		           - this.matrixArray[0][2] * this.matrixArray[1][1] * this.matrixArray[3][3] + this.matrixArray[0][1] * this.matrixArray[1][2] * this.matrixArray[3][3];
		double m03 = this.matrixArray[0][3] * this.matrixArray[1][2] * this.matrixArray[2][1] - this.matrixArray[0][2] * this.matrixArray[1][3] * this.matrixArray[2][1]
		           - this.matrixArray[0][3] * this.matrixArray[1][1] * this.matrixArray[2][2] + this.matrixArray[0][1] * this.matrixArray[1][3] * this.matrixArray[2][2]
		           + this.matrixArray[0][2] * this.matrixArray[1][1] * this.matrixArray[2][3] - this.matrixArray[0][1] * this.matrixArray[1][2] * this.matrixArray[2][3];
		double m10 = this.matrixArray[1][3] * this.matrixArray[2][2] * this.matrixArray[3][0] - this.matrixArray[1][2] * this.matrixArray[2][3] * this.matrixArray[3][0]
		           - this.matrixArray[1][3] * this.matrixArray[2][0] * this.matrixArray[3][2] + this.matrixArray[1][0] * this.matrixArray[2][3] * this.matrixArray[3][2]
		           + this.matrixArray[1][2] * this.matrixArray[2][0] * this.matrixArray[3][3] - this.matrixArray[1][0] * this.matrixArray[2][2] * this.matrixArray[3][3];
		double m11 = this.matrixArray[0][2] * this.matrixArray[2][3] * this.matrixArray[3][0] - this.matrixArray[0][3] * this.matrixArray[2][2] * this.matrixArray[3][0]
		           + this.matrixArray[0][3] * this.matrixArray[2][0] * this.matrixArray[3][2] - this.matrixArray[0][0] * this.matrixArray[2][3] * this.matrixArray[3][2]
		           - this.matrixArray[0][2] * this.matrixArray[2][0] * this.matrixArray[3][3] + this.matrixArray[0][0] * this.matrixArray[2][2] * this.matrixArray[3][3];
		double m12 = this.matrixArray[0][3] * this.matrixArray[1][2] * this.matrixArray[3][0] - this.matrixArray[0][2] * this.matrixArray[1][3] * this.matrixArray[3][0]
		           - this.matrixArray[0][3] * this.matrixArray[1][0] * this.matrixArray[3][2] + this.matrixArray[0][0] * this.matrixArray[1][3] * this.matrixArray[3][2]
		           + this.matrixArray[0][2] * this.matrixArray[1][0] * this.matrixArray[3][3] - this.matrixArray[0][0] * this.matrixArray[1][2] * this.matrixArray[3][3];
		double m13 = this.matrixArray[0][2] * this.matrixArray[1][3] * this.matrixArray[2][0] - this.matrixArray[0][3] * this.matrixArray[1][2] * this.matrixArray[2][0]
		           + this.matrixArray[0][3] * this.matrixArray[1][0] * this.matrixArray[2][2] - this.matrixArray[0][0] * this.matrixArray[1][3] * this.matrixArray[2][2]
		           - this.matrixArray[0][2] * this.matrixArray[1][0] * this.matrixArray[2][3] + this.matrixArray[0][0] * this.matrixArray[1][2] * this.matrixArray[2][3];
		double m20 = this.matrixArray[1][1] * this.matrixArray[2][3] * this.matrixArray[3][0] - this.matrixArray[1][3] * this.matrixArray[2][1] * this.matrixArray[3][0]
		           + this.matrixArray[1][3] * this.matrixArray[2][0] * this.matrixArray[3][1] - this.matrixArray[1][0] * this.matrixArray[2][3] * this.matrixArray[3][1]
		           - this.matrixArray[1][1] * this.matrixArray[2][0] * this.matrixArray[3][3] + this.matrixArray[1][0] * this.matrixArray[2][1] * this.matrixArray[3][3];
		double m21 = this.matrixArray[0][3] * this.matrixArray[2][1] * this.matrixArray[3][0] - this.matrixArray[0][1] * this.matrixArray[2][3] * this.matrixArray[3][0]
		           - this.matrixArray[0][3] * this.matrixArray[2][0] * this.matrixArray[3][1] + this.matrixArray[0][0] * this.matrixArray[2][3] * this.matrixArray[3][1]
		           + this.matrixArray[0][1] * this.matrixArray[2][0] * this.matrixArray[3][3] - this.matrixArray[0][0] * this.matrixArray[2][1] * this.matrixArray[3][3];
		double m22 = this.matrixArray[0][1] * this.matrixArray[1][3] * this.matrixArray[3][0] - this.matrixArray[0][3] * this.matrixArray[1][1] * this.matrixArray[3][0]
		           + this.matrixArray[0][3] * this.matrixArray[1][0] * this.matrixArray[3][1] - this.matrixArray[0][0] * this.matrixArray[1][3] * this.matrixArray[3][1]
		           - this.matrixArray[0][1] * this.matrixArray[1][0] * this.matrixArray[3][3] + this.matrixArray[0][0] * this.matrixArray[1][1] * this.matrixArray[3][3];
		double m23 = this.matrixArray[0][3] * this.matrixArray[1][1] * this.matrixArray[2][0] - this.matrixArray[0][1] * this.matrixArray[1][3] * this.matrixArray[2][0]
		           - this.matrixArray[0][3] * this.matrixArray[1][0] * this.matrixArray[2][1] + this.matrixArray[0][0] * this.matrixArray[1][3] * this.matrixArray[2][1]
		           + this.matrixArray[0][1] * this.matrixArray[1][0] * this.matrixArray[2][3] - this.matrixArray[0][0] * this.matrixArray[1][1] * this.matrixArray[2][3];
		double m30 = this.matrixArray[1][2] * this.matrixArray[2][1] * this.matrixArray[3][0] - this.matrixArray[1][1] * this.matrixArray[2][2] * this.matrixArray[3][0]
		           - this.matrixArray[1][2] * this.matrixArray[2][0] * this.matrixArray[3][1] + this.matrixArray[1][0] * this.matrixArray[2][2] * this.matrixArray[3][1]
		           + this.matrixArray[1][1] * this.matrixArray[2][0] * this.matrixArray[3][2] - this.matrixArray[1][0] * this.matrixArray[2][1] * this.matrixArray[3][2];
		double m31 = this.matrixArray[0][1] * this.matrixArray[2][2] * this.matrixArray[3][0] - this.matrixArray[0][2] * this.matrixArray[2][1] * this.matrixArray[3][0]
		           + this.matrixArray[0][2] * this.matrixArray[2][0] * this.matrixArray[3][1] - this.matrixArray[0][0] * this.matrixArray[2][2] * this.matrixArray[3][1]
		           - this.matrixArray[0][1] * this.matrixArray[2][0] * this.matrixArray[3][2] + this.matrixArray[0][0] * this.matrixArray[2][1] * this.matrixArray[3][2];
		double m32 = this.matrixArray[0][2] * this.matrixArray[1][1] * this.matrixArray[3][0] - this.matrixArray[0][1] * this.matrixArray[1][2] * this.matrixArray[3][0]
		           - this.matrixArray[0][2] * this.matrixArray[1][0] * this.matrixArray[3][1] + this.matrixArray[0][0] * this.matrixArray[1][2] * this.matrixArray[3][1]
		           + this.matrixArray[0][1] * this.matrixArray[1][0] * this.matrixArray[3][2] - this.matrixArray[0][0] * this.matrixArray[1][1] * this.matrixArray[3][2];
		double m33 = this.matrixArray[0][1] * this.matrixArray[1][2] * this.matrixArray[2][0] - this.matrixArray[0][2] * this.matrixArray[1][1] * this.matrixArray[2][0]
		           + this.matrixArray[0][2] * this.matrixArray[1][0] * this.matrixArray[2][1] - this.matrixArray[0][0] * this.matrixArray[1][2] * this.matrixArray[2][1]
		           - this.matrixArray[0][1] * this.matrixArray[1][0] * this.matrixArray[2][2] + this.matrixArray[0][0] * this.matrixArray[1][1] * this.matrixArray[2][2];

		double inverseDeterminant = 1.0d / determinant;
		
		output.matrixArray[0][0] = m00 * inverseDeterminant;
		output.matrixArray[1][0] = m10 * inverseDeterminant;
		output.matrixArray[2][0] = m20 * inverseDeterminant;
		output.matrixArray[3][0] = m30 * inverseDeterminant;
		output.matrixArray[0][1] = m01 * inverseDeterminant;
		output.matrixArray[1][1] = m11 * inverseDeterminant;
		output.matrixArray[2][1] = m21 * inverseDeterminant;
		output.matrixArray[3][1] = m31 * inverseDeterminant;
		output.matrixArray[0][2] = m02 * inverseDeterminant;
		output.matrixArray[1][2] = m12 * inverseDeterminant;
		output.matrixArray[2][2] = m22 * inverseDeterminant;
		output.matrixArray[3][2] = m32 * inverseDeterminant;
		output.matrixArray[0][3] = m03 * inverseDeterminant;
		output.matrixArray[1][3] = m13 * inverseDeterminant;
		output.matrixArray[2][3] = m23 * inverseDeterminant;
		output.matrixArray[3][3] = m33 * inverseDeterminant;
		
		return output;
	}
}
