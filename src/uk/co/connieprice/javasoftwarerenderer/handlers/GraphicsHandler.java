package uk.co.connieprice.javasoftwarerenderer.handlers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import uk.co.connieprice.javasoftwarerenderer.objects.Camera;
import uk.co.connieprice.javasoftwarerenderer.objects.Light;
import uk.co.connieprice.javasoftwarerenderer.objects.Object3D;

/**
 * <h1>GraphicsHandler</h1>
 * The main graphics handler. Handles the graphical representation side of things.
 * 
 * @author Connie Price
 *
 */
public class GraphicsHandler implements Runnable {
	private JFrame window;
	private TestSurface panel;

	/**
	 * Create the graphics handler instance.
	 * @param title The title of the graphics window.
	 */
	public GraphicsHandler(String title, ObjectHandler objectHandler, int width, int height) {
		this.window = new JFrame();

		this.window.setTitle(title);
		this.window.setResizable(false);

		this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.panel = new TestSurface(objectHandler, width, height);
		this.window.add(this.panel);

		this.window.pack();
		
		this.panel.init();
	}

	private long framerateInterval = 1000/60; // About 60 frames per second.
	
	@Override
	public void run() {
		this.window.setVisible(true);

		while(true) {
			this.panel.render();
						
//			long startTime = System.currentTimeMillis();
//
//			this.panel.repaint();
//
//			long endTime = System.currentTimeMillis();
//			long deltaTime = endTime - startTime;
//
//			try {
//				long delay = framerateInterval - deltaTime;
//				if (delay > 0) { // If we are slower than the target frame rate than just don't delay, keep going.
//					Thread.sleep(framerateInterval - deltaTime);
//				}
//			} catch (InterruptedException e) {
//				System.out.print("Thread Interrupted");
//			}
		}
	}

	/**
	 * Set the active camera.
	 * @param camera The camera to use as the active camera.
	 */
	public void setActiveCamera(Camera camera) {
		this.panel.activeCamera = camera;
	}

	/**
	 * Get the active camera.
	 * @return The active camera.
	 */
	public Camera getActiveCamera() {
		return this.panel.activeCamera;
	}
}

class TestSurface extends Canvas {
	protected Camera activeCamera;
	private ObjectHandler objectHandler;

	private int width;
	private int height;
	
	BufferedImage canvas;
	int[] canvasData;
	
	private int bufferCount = 2;
	private int[][] buffers;
	private int[] bufferClearValues = {
		Color.BLACK.getRGB(),
		Integer.MIN_VALUE
	};
	private int[][] clearedBuffers;
	
	public TestSurface(ObjectHandler objectHandler, int width, int height) {
		this.setFocusable(true);
		this.setPreferredSize(new Dimension(width, height));
		this.requestFocusInWindow();

		this.objectHandler = objectHandler;
		
		this.width = width;
		this.height = height;
		
		this.canvas = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		this.canvasData = ((DataBufferInt) canvas.getRaster().getDataBuffer()).getData();
		
		this.buffers = new int[bufferCount][];
		this.clearedBuffers = new int[bufferCount][];
		
		for (int i = bufferCount - 1; i >= 0; i--) {
			this.buffers[i] = new int[width * height];
			this.clearedBuffers[i] = new int[width * height];
		}
		
		for ( int row = 0; row < height; row++ ) {
			for ( int column = 0; column < width; column++ ) {
				int index = column + width*row;
				
				for (int i = bufferCount - 1; i >= 0; i--) {
					this.buffers[i][index] = bufferClearValues[i];
					this.clearedBuffers[i][index] = bufferClearValues[i];
				}
			}
		}
	}
	
	public void init() {
		this.createBufferStrategy(3);
	}
	
	double delta = 0D;
	long last = System.nanoTime();
	long timer = 0L;
	
	String[] debugText = {
		"Framerate: Unknown",
		"Triangles: Unknown"
	};
	
	public void render() {
		BufferStrategy bufferStrategy = this.getBufferStrategy();
		if (bufferStrategy == null) return;
		
		Graphics graphics = bufferStrategy.getDrawGraphics();

		List<Object3D> objects = this.objectHandler.getObjects();
		List<Light> lights = this.objectHandler.getLights();
		int arrayLength = width*height;

		for (int i = bufferCount - 1; i >= 0; i--) {
			System.arraycopy(this.clearedBuffers[i], 0, this.buffers[i], 0, arrayLength);
		}

		int triangles = 0;
		
		// Set i to the size and go backwards as this is marginally
		// faster than constantly comparing against the size every time.
		for (int i = objects.size() - 1; i >= 0; i--) {
			Object3D object = objects.get(i);
			triangles += object.render(this.activeCamera, lights, this.buffers);
		}

		System.arraycopy(this.buffers[0], 0, canvasData, 0, arrayLength);

		graphics.drawImage(canvas, 0, height, width, -height, null);
		graphics.setColor(Color.WHITE);
		
		long now = System.nanoTime();
		long since = now - last;

		delta = since / 1000000000D;
		timer += since;
		last = now;
		
		long interval = (long) (0.25 * 1000000000L);
		if (timer >= interval) {
			debugText[0] = "FPS: " + (int) (1/delta);
			debugText[1] = "Triangles: " + triangles;

		    timer = 0L;
		}
		
		for (int i = debugText.length - 1; i >= 0; i--) {
			graphics.drawString(debugText[i], 10, 20 + (20 * i));
		}

        graphics.dispose();
        bufferStrategy.show();
	}
}

/**
 * <h1>Surface</h1>
 * A small custom JPanel that renders models and stores the current camera.
 *
 * @author Connie Price
 *
 */
@SuppressWarnings("serial") // We're not going to be serialising instances of this class, this warning is annoying.
class Surface extends JPanel {
	protected Camera activeCamera;
	private ObjectHandler objectHandler;

	private int width;
	private int height;

	BufferedImage canvas;
	int[] canvasData;
	
	private int bufferCount = 2;
	private int[][] buffers;
	private int[] bufferClearValues = {
		Color.BLACK.getRGB(),
		65536
	};
	private int[][] clearedBuffers;

	Surface(ObjectHandler objectHandler, int width, int height) {
		this.setFocusable(true);
		this.setPreferredSize(new Dimension(width, height));
		this.requestFocusInWindow();

		this.objectHandler = objectHandler;
		
		this.width = width;
		this.height = height;
		
		this.canvas = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		this.canvasData = ((DataBufferInt) canvas.getRaster().getDataBuffer()).getData();
		
		this.buffers = new int[bufferCount][];
		this.clearedBuffers = new int[bufferCount][];
		
		for (int i = bufferCount - 1; i >= 0; i--) {
			this.buffers[i] = new int[width * height];
			this.clearedBuffers[i] = new int[width * height];
		}
		
		for ( int row = 0; row < height; row++ ) {
			for ( int column = 0; column < width; column++ ) {
				int index = column + width*row;
				
				for (int i = bufferCount - 1; i >= 0; i--) {
					this.buffers[i][index] = bufferClearValues[i];
					this.clearedBuffers[i][index] = bufferClearValues[i];
				}
			}
		}
	}
	
	double delta = 0D;
	long last = System.nanoTime();
	long timer = 0L;
	
	String[] debugText = {
		"Framerate: Unknown",
		"Triangles: Unknown"
	};
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D graphics2D = (Graphics2D) g;

		List<Object3D> objects = this.objectHandler.getObjects();
		List<Light> lights = this.objectHandler.getLights();
		int arrayLength = width*height;

		for (int i = bufferCount - 1; i >= 0; i--) {
			System.arraycopy(this.clearedBuffers[i], 0, this.buffers[i], 0, arrayLength);
		}

		int triangles = 0;
		
		// Set i to the size and go backwards as this is marginally
		// faster than constantly comparing against the size every time.
		for (int i = objects.size() - 1; i >= 0; i--) {
			Object3D object = objects.get(i);
			triangles += object.render(this.activeCamera, lights, this.buffers);
		}

		System.arraycopy(this.buffers[0], 0, canvasData, 0, arrayLength);

		graphics2D.drawImage(canvas, 0, height, width, -height, null);
		graphics2D.setColor(Color.WHITE);
		
		long now = System.nanoTime();
		long since = now - last;

		delta = since / 1000000000D;
		timer += since;
		last = now;
		
		long interval = (long) (0.25 * 1000000000L);
		if (timer >= interval) {
			debugText[0] = "FPS: " + (int) (1/delta);
			debugText[1] = "Triangles: " + triangles;

		    timer = 0L;
		}
		
		for (int i = debugText.length - 1; i >= 0; i--) {
			graphics2D.drawString(debugText[i], 10, 20 + (20 * i));
		}
	}
}