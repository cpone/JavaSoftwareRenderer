package uk.co.connieprice.javasoftwarerenderer;

import uk.co.connieprice.javasoftwarerenderer.handlers.GraphicsHandler;
import uk.co.connieprice.javasoftwarerenderer.handlers.ObjectHandler;
import uk.co.connieprice.javasoftwarerenderer.objects.Camera;
import uk.co.connieprice.javasoftwarerenderer.objects.Light;
import uk.co.connieprice.javasoftwarerenderer.objects.Model.Vertex;
import uk.co.connieprice.javasoftwarerenderer.objects.MovingCamera;
import uk.co.connieprice.javasoftwarerenderer.objects.SpinningModel;
import uk.co.connieprice.javasoftwarerenderer.readers.OBJReader;
import uk.co.connieprice.javasoftwarerenderer.math.ColorVector;
import uk.co.connieprice.javasoftwarerenderer.math.Euler;

/**
 * <h1>Main</h1>
 * The 'Main' class is the central class to build up the front of the application.
 * 
 * @author Connie Price
 *
 */
public class Main {
	public static ObjectHandler objectHandler;
	public static GraphicsHandler graphicsHandler;

	public static final int multiplier = 1;
	public static final int windowWidth = 640 * multiplier;
	public static final int windowHeight = 480 * multiplier;

	/**
	 * The main method. The program starts here and this sets up the basics for the game.
	 * @param args Command line arguments.
	 */
	public static void main(String[] args) {
		objectHandler = new ObjectHandler();
		Thread objectThread = new Thread(objectHandler);

		graphicsHandler = new GraphicsHandler("Java Software Renderer", objectHandler, windowWidth, windowHeight);
		Thread graphicsThread = new Thread(graphicsHandler);

		objectThread.start();
		graphicsThread.start();

		setupObject();
	}

	/**
	 * A method that simply sets up a camera and cube and adds them to the object handler.
	 */
	public static void setupObject() {
		double cameraHeight = 5;
		double cameraFOV = 80 * (Math.PI/180);
		double aspectRatio = (double) windowWidth / (double) windowHeight;

//		Camera camera = new MovingCamera().SetOrthographic(cameraHeight * aspectRatio, cameraHeight, 0, 500);
		Camera camera = new MovingCamera().SetPerspective(cameraFOV, aspectRatio, 0, 100);
		camera.setPosition(0, 0, -300);
		camera.updateTransformationMatrix();
		
		objectHandler.addObject(camera);
		graphicsHandler.setActiveCamera(camera);
		
		Light light = new Light(ColorVector.RED, 0, 3);
		light.setPosition(1, 1, 0);
		objectHandler.addObject(light);

		SpinningModel model = new SpinningModel();
		OBJReader.readOBJ(model, "f16.obj");
		objectHandler.addObject(model);
	}
}