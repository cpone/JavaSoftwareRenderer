package uk.co.connieprice.javasoftwarerenderer.objects;

import uk.co.connieprice.javasoftwarerenderer.math.Matrix4;
import uk.co.connieprice.javasoftwarerenderer.math.Vector2;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;

/**
 * <h1>Camera</h1>
 * A camera object which will store camera relative info
 * as well as helper methods for position translation.
 *
 * @author Connie Price
 *
 */
public class Camera extends Object3D {
	Matrix4 projectionMatrix = Matrix4.TRANSFORMLESS.copy();
	Matrix4 inverseProjectionMatrix = Matrix4.TRANSFORMLESS.copy();
	
	Matrix4 inverseTransformationMatrix = Matrix4.TRANSFORMLESS.copy();
	
	public Camera SetOrthographic(double width, double height, double near, double far) {
		this.projectionMatrix = Matrix4.OrthographicProjection(height/2, -height/2, -width/2, width/2, near, far);
		this.inverseProjectionMatrix = this.projectionMatrix.inverse();
		
		return this;
	}
	
	public Camera SetPerspective(double FOV, double aspectRatio, double near, double far) {
		this.projectionMatrix = Matrix4.PerspectiveProjection(FOV, aspectRatio, near, far);
		this.inverseProjectionMatrix = this.projectionMatrix.inverse();

		return this;
	}
	
	@Override
	public void updateTransformationMatrix() {
		super.updateTransformationMatrix();

		inverseTransformationMatrix = this.transformationMatrix.inverse();
	}
	
	public Vector3 worldToScreen(Vector3 worldPosition) {
		Vector3 output = worldPosition.copy().multiply(this.inverseTransformationMatrix).multiply(this.projectionMatrix);

		return output;
	}

	public Vector3 screenToWorld(Vector3 screenPosition) {
		return screenPosition.copy().multiply(this.inverseProjectionMatrix).multiply(this.transformationMatrix);
	}
}
