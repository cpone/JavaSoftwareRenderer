package uk.co.connieprice.javasoftwarerenderer.objects;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;

import uk.co.connieprice.javasoftwarerenderer.math.Euler;
import uk.co.connieprice.javasoftwarerenderer.math.Matrix4;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;

/**
 * <h1>Object3D</h1>
 * An empty helper object to act as a basis of other types of objects.
 *
 * @author Connie Price
 *
 */
public class Object3D {
	public Vector3 position = new Vector3();
	public Euler rotation = new Euler();
	public Vector3 scale = new Vector3(1, 1, 1);

	public Matrix4 transformationMatrix = Matrix4.TRANSFORMLESS.copy();
	public Matrix4 transInvTransformationMatrix = Matrix4.TRANSFORMLESS.copy();
	
	private boolean transformationMatrixUpdateWanted = true;
	public void setTransformationMatrixUpdateWanted() {
		transformationMatrixUpdateWanted = true;
	}
	
	public void updateTransformationMatrix() {
		Matrix4 translationMatrix = Matrix4.FromTranslation(position);
		Matrix4 rotationMatrix = Matrix4.FromEuler(rotation);
		Matrix4 scaleMatrix = Matrix4.FromScale(scale);

		transformationMatrix = translationMatrix.multiply(rotationMatrix).multiply(scaleMatrix);
		transInvTransformationMatrix = transformationMatrix.inverse().transpose();
	}

	public Object3D setPosition(double x, double y, double z) {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
		
		return this;
	}
	
	public Object3D setPosition(Vector3 position) {
		return this.setPosition(position.x, position.y, position.z);
	}
	
	public Object3D setRotation(double pitch, double yaw, double roll) {
		this.rotation.pitch = pitch;
		this.rotation.yaw = yaw;
		this.rotation.roll = roll;
		
		return this;
	}
	
	public Object3D setRotation(Euler rotation) {
		return this.setRotation(rotation.pitch, rotation.yaw, rotation.roll);
	}
	
	public Object3D setScale(double x, double y, double z) {
		this.scale.x = x;
		this.scale.y = y;
		this.scale.z = z;
		
		return this;
	}
	
	public Object3D setScale(Vector3 scale) {
		return this.setPosition(scale.x, scale.y, scale.z);
	}
	
	/**
	 * Render the object.
	 * @param camera The camera to render from.
	 * @param graphics2D The graphics object to render to.
	 */
	public int render(Camera camera, List<Light> lights, int[][] buffers) {
		if(transformationMatrixUpdateWanted) {
			this.updateTransformationMatrix();
		}

		return 0;
	}

	/**
	 * Update the object.
	 * @param time The current time. (Milliseconds)
	 * @param deltaTime The time since the last update. (Milliseconds)
	 */
	public void update(long time, long deltaTime) {}
}
