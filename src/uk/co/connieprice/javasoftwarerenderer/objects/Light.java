package uk.co.connieprice.javasoftwarerenderer.objects;

import uk.co.connieprice.javasoftwarerenderer.math.ColorVector;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;

public class Light extends Object3D {
	ColorVector color;

	double falloffStart = 0;
	double falloffEnd = 0;
	
	public Light(ColorVector color, double falloffStart, double falloffEnd) {
		this.color = color;
		this.falloffStart = falloffStart;
		this.falloffEnd = falloffEnd;
	}
	
	public ColorVector GetLightColor(Vector3 worldLocation) {
		double distance = worldLocation.distance(position);
		if (distance > falloffEnd) {
			return ColorVector.BLACK;
		}
		
		if (distance < falloffStart) {
			return color;
		}
		
		double falloffBlend = (distance - falloffStart) / (falloffEnd - falloffStart);
		return color.copy().lerp(ColorVector.BLACK, falloffBlend);
	}
}
