package uk.co.connieprice.javasoftwarerenderer.objects;

import java.util.ArrayList;
import java.util.List;

import uk.co.connieprice.javasoftwarerenderer.shaders.DiffuseShader;
import uk.co.connieprice.javasoftwarerenderer.shaders.NormalShader;
import uk.co.connieprice.javasoftwarerenderer.shaders.Shader;
import uk.co.connieprice.javasoftwarerenderer.shaders.UVShader;
import uk.co.connieprice.javasoftwarerenderer.shaders.UnlitDiffuseShader;

/**
 * <h1>SpinningModel</h1>
 * An extension of a model that just spins. :D
 * 
 * @author Connie Price
 *
 */
public class SpinningModel extends Model {
	double spinSpeed = 12;
	double alternateTime = 5;
	List<Shader> alternatingShaders = new ArrayList<Shader>();

	public SpinningModel() {
		alternatingShaders.add(new DiffuseShader());
		alternatingShaders.add(new UnlitDiffuseShader());
		alternatingShaders.add(new UVShader());
		alternatingShaders.add(new NormalShader());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(long time, long deltaTime) {
		double seconds = time/1000d;
		double angle = seconds * spinSpeed * (Math.PI/180);

//		this.setRotation(0, Math.PI/4, 0);
		this.setRotation(0, angle, 0);

		double scale = 1;
		this.setScale(scale, scale, scale);

		setTransformationMatrixUpdateWanted();

		int shaderIndex = (int) Math.floor(seconds/alternateTime % this.alternatingShaders.size());
		for (Material material : this.materials.values()) {
			material.shader = this.alternatingShaders.get(shaderIndex);
		}
	}
}
