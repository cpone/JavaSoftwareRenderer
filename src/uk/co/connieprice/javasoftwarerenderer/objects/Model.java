package uk.co.connieprice.javasoftwarerenderer.objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import uk.co.connieprice.javasoftwarerenderer.Main;
import uk.co.connieprice.javasoftwarerenderer.math.ColorVector;
import uk.co.connieprice.javasoftwarerenderer.math.Euler;
import uk.co.connieprice.javasoftwarerenderer.math.Matrix;
import uk.co.connieprice.javasoftwarerenderer.math.Vector2;
import uk.co.connieprice.javasoftwarerenderer.math.Vector3;
import uk.co.connieprice.javasoftwarerenderer.shaders.Shader;

/**
 * <h1>Model</h1>
 * An Object that can store and render model data.
 * 
 * @author Connie Price
 *
 */
public class Model extends Object3D {
	/**
	 * <h1>Vertex</h1>
	 * A vertex is essentially a point on the model, for now it only stores
	 * a position but we may want it to store more vertex unique data
	 * in the future. 
	 * 
	 * @author Connie Price
	 *
	 */
	static public class Vertex {
		public final Vector3 position;
		public final Vector3 uvs;
		public final Vector3 normals;
		
		public final ColorVector vertexColor;

		/**
		 * Create a vertex.
		 * @param x X coordinate.
		 * @param y Y coordinate.
		 * @param z Z coordinate.
		 */
		public Vertex(double x, double y, double z) {
			this.position = new Vector3(x, y, z);
			this.uvs = new Vector3(0, 0, 0);
			this.normals = new Vector3(0, 0, 0);
			this.vertexColor = ColorVector.RED;
		}
		
		public Vertex(Vector3 position, Vector3 uvs, Vector3 normals) {
			this.position = position;
			this.uvs = uvs;
			this.normals = normals;
			
			this.vertexColor = ColorVector.WHITE;
		}
	}

	/**
	 * <h1>Triangle</h1>
	 * An edge is just a way to store a "line" between two vertexes.
	 * 
	 * @author Connie Price
	 * 
	 */
	static public class Triangle {
		public final Vertex vertex1;
		public final Vertex vertex2;
		public final Vertex vertex3;
		
		public String material;
		
		/**
		 * Create an edge.
		 * @param vertex1 The first vertex.
		 * @param vertex2 The second vertex.
		 */
		public Triangle(Vertex vertex1, Vertex vertex2, Vertex vertex3, String material) {
			this.vertex1 = vertex1;
			this.vertex2 = vertex2;
			this.vertex3 = vertex3;
			this.material = material;
		}
	}

	public List<Triangle> triangles = new ArrayList<Triangle>();

	/**
	 * Add a triangle to the model.
	 * @param triangle The triangle to add.
	 */
	public void addTriangle(Triangle triangle) {
		this.triangles.add(triangle);
	}

	static public class Material {
		Shader shader;
		BufferedImage defaultTexture = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		HashMap<String, BufferedImage> textures = new HashMap<String, BufferedImage>();
		
		public Material() {
			this.shader = new Shader();
		};
		
		public Material(Shader shader) {
			this.shader = shader;
		};
		
		public void addTexture(String name, String path) throws IOException {
			File file = new File(path);
			BufferedImage textureTemp = ImageIO.read(file);
			
			BufferedImage textureOut = new BufferedImage(textureTemp.getWidth(), textureTemp.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = textureOut.createGraphics();
	        g.drawImage(textureTemp, 0, textureTemp.getHeight(), textureTemp.getWidth(), -textureTemp.getHeight(), null);
	        g.dispose();
	        
			this.textures.put(name, textureOut);
		}
		
		public BufferedImage getTexture(String name) {
			return this.textures.getOrDefault(name, defaultTexture);
		}
	}
	
	public HashMap<String, Material> materials = new HashMap<String, Material>();
	
	public void addMaterial(String materialName, Material material) {
		materials.put(materialName, material);
	}
	
	public void drawLine(int[][] buffers, Vector3 point1, Vector3 point2, Color color) {
		int colorInt = color.getRGB();

		int p1x = (int) point1.x;
		int p1y = (int) point1.y;
		
		int p2x = (int) point2.x;
		int p2y = (int) point2.y;
		
		if (Math.abs(p1x - p2x) < Math.abs(p1y - p2y)) {
			for (int y = p1y; y <= p2y; y++) {
				double t = (y - p1y) / (double) (p2y - p1y);
				int x = (int) (p1x * (1 - t) + p2x * t);

				int index = x + Main.windowWidth*y;
				buffers[0][index] = color.getRGB();
			}

		} else {
			for (int x = p1x; x <= p2x; x++) {
				double t = (x - p1x) / (double) (p2x - p1x);
				int y = (int) (p1y * (1 - t) + p2y * t);

				int index = x + Main.windowWidth*y;
				buffers[0][index] = color.getRGB();
			}
		}
	}
	
	static public class IntersectionResult {
		public final boolean intersection;
		public final double u;
		public final double v;
		public final double w;
		
		public IntersectionResult(boolean intersection, double u, double v, double w) {
			this.intersection = intersection;
			this.u = u;
			this.v = v;
			this.w = w;
		}
	}
	
	private final double EPSILON = 0.0000001;
	public IntersectionResult rayIntersection(Vector3 vector1, Vector3 vector2, Vector3 vector3, Vector3 origin, Vector3 direction) {		
		Vector3 edge1 = new Vector3();
		Vector3 edge2 = new Vector3();
		
		Vector3 h = new Vector3();
		Vector3 s = new Vector3();
		Vector3 q = new Vector3();

		edge1.set(vector2).subtract(vector1);
		edge2.set(vector3).subtract(vector1);
		
		h.set(direction).crossProduct(edge2);
		double determinant = edge1.dotProduct(h);
		
		if (determinant < EPSILON) {
			return new IntersectionResult(false, 0, 0, 1);
		}
		
		double u, v;
		double invertedDeterminant = 1.0 / determinant;
		
		s.set(origin).subtract(vector1);
		u = invertedDeterminant * s.dotProduct(h);
		if (u < 0.0 || u > 1.0) {
			return new IntersectionResult(false, u, 0, 1 - u);
		}
		
		q.set(s).crossProduct(edge1);
		v = invertedDeterminant * direction.dotProduct(q);
		if (v < 0.0 || u + v > 1.0) {
			return new IntersectionResult(false, u, v, 1 - u - v);
		}
		
		double t = invertedDeterminant * edge2.dotProduct(q);
		return new IntersectionResult(t > EPSILON, u, v, 1 - u - v);
	}
	
	public Vector3 bufferSize = new Vector3(Main.windowWidth, Main.windowHeight, 65536);
	public Vector3 halfBufferSize = bufferSize.copy().divide(2);
	
	public void drawTriangle(int[][] buffers, Camera camera, List<Light> lights, Triangle triangle, Color color) {
		Vector3 cameraDirection = camera.rotation.forward();
		
		Vector3 v1 = triangle.vertex1.position.copy().multiply(this.transformationMatrix);
		Vector3 v2 = triangle.vertex2.position.copy().multiply(this.transformationMatrix);
		Vector3 v3 = triangle.vertex3.position.copy().multiply(this.transformationMatrix);

		Vector3 point1 = camera.worldToScreen(v1);
		Vector3 point2 = camera.worldToScreen(v2);
		Vector3 point3 = camera.worldToScreen(v3);
		
		if (
			Math.abs(point1.x) > 1 || Math.abs(point1.y) > 1 ||
			Math.abs(point2.x) > 1 || Math.abs(point2.y) > 1 ||
			Math.abs(point3.x) > 1 || Math.abs(point3.y) > 1
		) return;
		
		point1.add(1).multiply(halfBufferSize);
		point2.add(1).multiply(halfBufferSize);
		point3.add(1).multiply(halfBufferSize);
		
		int minX = (int) Math.min(Math.min(point1.x, point2.x), point3.x);
		int maxX = (int) Math.max(Math.max(point1.x, point2.x), point3.x);

		int minY = (int) Math.min(Math.min(point1.y, point2.y), point3.y);
		int maxY = (int) Math.max(Math.max(point1.y, point2.y), point3.y);

		Vector3 tempVector1 = new Vector3();
		Vector3 tempVector2 = new Vector3();
		Vector3 tempVector3 = new Vector3();

		Vector3 surfaceNormal = new Vector3();
		
		Vector3 vWorld = new Vector3();
		Vector3 vNormal = new Vector3();
		Vector3 vUV = new Vector3();
		
		Vector3 lightDirection = new Vector3();
		ColorVector lightOutput = new ColorVector();
		
		ColorVector tempColorVector1 = new ColorVector();
		ColorVector tempColorVector2 = new ColorVector();
		ColorVector tempColorVector3 = new ColorVector();

		ColorVector vPixel = new ColorVector();
		
		Vector3 direction = camera.rotation.forward();
		
		Vector3 screenProj = new Vector3();
		Vector3 origin;
		IntersectionResult result;

		Material material = this.materials.get(triangle.material);
		Shader shader;
		if (material != null) {
			shader = material.shader;
		} else {
			shader = new Shader();
		}
		
//		Vector3 normal1WorldEnd = v1.copy().add(triangle.vertex1.normals.copy().multiply(this.transInvTransformationMatrix).multiply(0.1));
//		Vector3 normal1End = camera.worldToScreen(normal1WorldEnd).add(1).multiply(halfBufferSize);
//		drawLine(buffers, point1, normal1End, Color.RED);
//		
//		Vector3 normal2WorldEnd = v2.copy().add(triangle.vertex2.normals.copy().multiply(this.transInvTransformationMatrix).multiply(0.1));
//		Vector3 normal2End = camera.worldToScreen(normal2WorldEnd).add(1).multiply(halfBufferSize);
//		drawLine(buffers, point2, normal2End, Color.GREEN);
//		
//		Vector3 normal3WorldEnd = v3.copy().add(triangle.vertex3.normals.copy().multiply(this.transInvTransformationMatrix).multiply(0.1));
//		Vector3 normal3End = camera.worldToScreen(normal3WorldEnd).add(1).multiply(halfBufferSize);
//		drawLine(buffers, point3, normal3End, Color.BLUE);
		
		tempVector1.set(v2).subtract(v1);
		tempVector2.set(v3).subtract(v1);
		surfaceNormal.set(tempVector1.crossProduct(tempVector2)).normalize().multiply(-1);

		for (int x = minX; x <= maxX; x += 1) {
			boolean lastWasIntersection = false;
			for (int y = minY; y <= maxY; y += 1) {
				screenProj.set(x, y, 0).divide(halfBufferSize).subtract(1);
				origin = camera.screenToWorld(screenProj);

				result = this.rayIntersection(v1, v2, v3, origin, direction);
//				result = new IntersectionResult(true, 0, 0, 1);
				if (result.intersection) {
					lastWasIntersection = true;

					int index = x + Main.windowWidth*y;
					int currentDepth = buffers[1][index];
					int intersectionDepth = (int) (result.u * point2.z + result.v * point3.z + result.w * point1.z);

					if (intersectionDepth > currentDepth) {
						tempVector1.set(v1).multiply(result.w);
						tempVector2.set(v2).multiply(result.u);
						tempVector3.set(v3).multiply(result.v);
						vWorld.set(tempVector1).add(tempVector2).add(tempVector3);
						
						tempVector1.set(triangle.vertex1.normals).multiply(result.w);
						tempVector2.set(triangle.vertex2.normals).multiply(result.u);
						tempVector3.set(triangle.vertex3.normals).multiply(result.v);
						vNormal.set(tempVector1).add(tempVector2).add(tempVector3);

						if (vNormal.squareLength() <= EPSILON) {
							vNormal.set(surfaceNormal);
						}
//						
////						vNormal.multiply(this.transInvTransformationMatrix);
//
						tempVector1.set(triangle.vertex1.uvs).multiply(result.w);
						tempVector2.set(triangle.vertex2.uvs).multiply(result.u);
						tempVector3.set(triangle.vertex3.uvs).multiply(result.v);
						vUV.set(tempVector1).add(tempVector2).add(tempVector3);
//						
						lightOutput.setZero();
						lightDirection.setZero();
						for (Light light : lights) {
							lightOutput.add(light.GetLightColor(vWorld));
							lightDirection.add(light.position.vectorDistance(vWorld).normalize());
						}
						lightDirection.divide(lights.size());
						lightDirection.normalize();
//						
						tempColorVector1.set(triangle.vertex1.vertexColor).multiply(result.w);
						tempColorVector2.set(triangle.vertex2.vertexColor).multiply(result.u);
						tempColorVector3.set(triangle.vertex3.vertexColor).multiply(result.v);
						vPixel.set(tempColorVector1).add(tempColorVector2).add(tempColorVector3);
//
						shader.RenderPixel(vPixel, lightOutput, lightDirection, cameraDirection, vWorld, vNormal, vUV.x, vUV.y, material);
//						
						buffers[0][index] = vPixel.getARGB();
						buffers[1][index] = intersectionDepth;
					}
				} else if (lastWasIntersection) {
					break; // End early if we reach the edge of the current triangle.
				}
			}
		}
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int render(Camera camera, List<Light> lights, int[][] buffers) {
		super.render(camera, lights, buffers);

		int triangles = this.triangles.size();
		
		for (int i = triangles - 1; i >= 0; i--) {
			Triangle triangle = this.triangles.get(i);

			Color color = i % 2 == 0 ? Color.RED : Color.GREEN;
			drawTriangle(buffers, camera, lights, triangle, color);
		}
		
		return triangles;
	}
}